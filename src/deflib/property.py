'''
Created on Sep 12, 2012

@author: bulentk
'''

import copy

class Properties(object):
    __slots__ = ("__table__", "__import__")
    def __init__(self, table=None):
        self.__table__ = {}
        self.__import__ = set()
        
        if table:
            self.update(table)

    def addimport(self, s):
        self.__import__.add(s)
        
    def update(self, table):
        if isinstance(table, dict):
            self.__table__.update(copy.copy(table))
        elif isinstance(table, Properties):
            self.__table__.update(copy.copy(table.__table__))
            self.__import__.update(copy.copy(table.__import__))
        else:
            raise ValueError("invalid parameter type. required: dict | Properties type:" + type(table))
    def __eq__(self, that):
        return self.__cmp__(that) == 0
    
    def __cmp__(self, that):
        if not that:
            return 1
        
        result = 0
        if (len(self.__table__) == len(that.__table__)):
            for i in self.__table__.items():
                if not that.__table__.has_key(i[0]):
                    result = 1
                    break
                v = that.__table__.get(i[0])
                if type(i[1]) != type(v):
                    result = 1
                    break
                result = cmp(i[1], v)
                if result != 0:
                    break
        else:
            result = -1 if len(self.__table__) < len(that.__table__) else 1
            
        return result

    def __contains__(self, key):
        if key in self.__table__: 
            return True
        return False
    
    def __setattr__(self, name, value):
#         print "setattr", name, value
        if name in self.__slots__:
            return super(Properties, self).__setattr__(name, value)
        if name in dir(object):
            return super(Properties, self).__setattr__(name, value)
            
        self[name] = value
    
    def __getattr__(self, name):
#         print "getattr", name
        if name in self.__slots__:
            return super(Properties, self).__getattr__(name)
        return self[name]
    
    def __delattr__(self, name):
        if name in self.__slots__:
            return super(Properties, self).__delattr__(name)
        del self[name]
    
    def __setitem__(self, key, value):
#         print "setitem", key, value
        self.__table__[key] = value
    
    def __getitem__(self, key):
#         print "getitem", key
        if not self.__table__.has_key(key):
            raise AttributeError("Properties object has no attribute \'" + key + "\'")
        return self.__table__[key]
    
    def __delitem__(self, key):
#         print "delitem", key
        if self.__table__.has_key(key):
            del self.__table__[key]
        
    def __str__(self):
        return str(self.__table__)
    
    def __repr__(self, *args, **kwargs):
        return "Properties(%r)" % (self.__table__)


def to_file(filename, prop):
    f = None
    try:
        f = open(filename, "w")

        def __list(v):
            s = set()
            for t in v:
                s |= __value(t)
            return s

        def __dict(v):
            s = set()
            items = v.items()
            for t in items:
                s |= __value(t[1])
            return s

        def __value(v):
            s = set()
            if isinstance(v, Properties):
                s |= __prop(v)
            elif isinstance(v, (list, tuple, set)):
                s |= __list(v)
            elif isinstance(v, dict):
                s |= __dict(v)
            
            return s
            
        def __prop(p):
            _imps_ = p.__import__.copy()
            items = p.__table__.items()
            for t in items:
                _imps_ |= __value(t[1])
                    
            return _imps_

        imps = set()
        imps |= __prop(prop)

        lst = []
        for i in imps:
            lst.append(i)
            
        f.write("__imports__=%r\n\n" % (lst))
        
        items = prop.__table__.items()
        for k,v in items:
            f.write("%s=%r\n" % (k, v))
    finally:
        if f:
            f.close()

def from_file(filename):
    f = None
    
    mdls = { "Properties": Properties }
    lines = []
    try:
        f = open(filename, "r")
        lns = f.read().splitlines()
        for ln in lns:
            ln = ln.strip()
            if not ln:
                continue
            if ln.startswith("#"):
                continue
            i = ln.find("=")
            if i == -1:
                continue
            k = ln[:i].strip()
            v = ln[i+1:].strip()
            if k == "__imports__":
                for i in eval(v):
                    mdls[i] = __import__(i)
            else:
                lines.append((k, v))
    finally:
        if f:
            f.close()
    p = Properties()
    if lines:
        for k,v in lines:
            p[k] = eval(v, mdls)
            
    return p
