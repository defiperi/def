'''
Created on Nov 30, 2012

@author: bulentk
'''

import engine
import os
import deflib
import datetime
import platform
import pwd

__allpkginfos = {}
__latestpkginfos = {}

def lookup(prj, version, pkg, build_config = None, machine = None, cpu = None):
    global __allpkginfos
    global __latestpkginfos
    
    if not prj:
        raise Exception("Invalid project name !!!")
    if not isinstance(prj, str):
        raise Exception("Invalid project name !!!")

    if not pkg:
        raise Exception("Invalid pakage name !!!")
    if not isinstance(pkg, str):
        raise Exception("Invalid pakage name !!!")
    
    m = machine if machine else engine.opt.target_machine
    c = cpu if cpu else engine.opt.target_cpu
    b = build_config if build_config else engine.opt.build_config
    
    key = m + c + prj + str(version) + b + pkg
    pkginf = __allpkginfos.get(key)
    if not pkginf:
        try:
            pkginf = engine.repo.lookup(m, c, prj, version, b, pkg)
            if not isinstance(pkginf.version, deflib.pkg.Version):
                pkginf.version = deflib.pkg.Version(pkginf.version)
        except deflib.pkg.PkgNotFoundException, e:
            if build_config != "release":
                pkginf = engine.repo.lookup(m, c, prj, version, "release", pkg)
            else:
                raise e

        __allpkginfos[key] = pkginf
        key = m + c + prj + b + pkg
        latest = __latestpkginfos.get(key)
        if latest:
            if pkginf.version > latest.version:
                __latestpkginfos[key] = pkginf
        else:
            __latestpkginfos[key] = pkginf
    return pkginf

def download(owner, inf):
    target = os.path.join(engine.settings.cache, inf.machine, inf.cpu, inf.prj, str(inf.version.major), str(inf.version.minor), str(inf.version.revision), inf.build_config, inf.name)
    if not os.path.exists(target):
        os.makedirs(target)
    if not os.path.isdir(target):
        raise Exception("Invalid target. Target is not directory ..." + target)
    
    target = os.path.join(target, "pkg." + deflib.pkg.ext)
    if os.path.exists(target):
        if engine.settings.cache != engine.settings.repository:
            if inf.checksum != deflib.pkg.checksum(target):
                deflib.logtool("repo", "Downloding %s/%s -> %s (%s)" % (owner, inf.name, inf.version, inf.build_config), False)
                engine.repo.pull(inf, target)
            else:
                deflib.logtool("repo", "Uptodate %s/%s -> %s (%s)" % (owner, inf.name, inf.version, inf.build_config), False)
        else:
            deflib.logtool("repo", "Uptodate %s/%s -> %s (%s)" % (owner, inf.name, inf.version, inf.build_config), False)
    else:
        deflib.logtool("repo", "Downloding %s/%s -> %s (%s)" % (owner, inf.name, inf.version, inf.build_config), False)
        engine.repo.pull(inf, target)

    return target

def downloadall(owner, dependecy, machine=None, cpu=None):
    lst = []
    
    build_config = dependecy.build_config if dependecy.build_config else engine.opt.build_config
    
    def __isset__(prj, version, pkg):
        for i in lst:
            if (i.prj == prj and i.version == version and i.name == pkg):
                return True
        return False

    def __get(own, prj, version, pkg):
        if __isset__(prj, version, pkg):
            return
        pkginf = lookup(prj, version, pkg, build_config, machine, cpu)
        pkginf.path = download(own, pkginf)
        lst.append(pkginf)
        
        for rp, rn, rv  in pkginf.requires:
            if rn == dependecy.name and rp == dependecy.prj:
                continue;
            if prj != rp:
                own += "/" + rp + "/" + rn
            else:
                own += "/" + rn
            __get(own, rp, rv, rn)
    
    __get(owner+"/"+dependecy.prj+"/"+dependecy.name, dependecy.prj, dependecy.version, dependecy.name)
    
    return lst
    

# def lookupall(name, version = None, machine = None, cpu = None, variant = None):
#     lst = []
#     def __walk(_name, _version):
# #         def __pkgname(_n):
# #             if pkg_name.rfind("-devel") != len(pkg_name) - len("-devel"):
# #                 conf = ""
# #                 if engine.opt.build_config != "release":
# #                     conf = "-" + engine.opt.build_config
# #                 pkg_name += conf
#             
# #             print "pkg", pkg_name
# #             return pkg_name
#         inf = lookup(_name, _version, machine, cpu, variant)
#         lst.append(inf)
# 
#         for rn, rv in inf.requires:
#             if __isset__(lst, rn, rv):
#                 continue
#             if rn == name:
#                 continue;
#             __walk(rn, rv)
#     
#     __walk(name, version)
#     
#     return lst

def upload(path, **kw):
    def __get_name__():
        # TODO fix name
        return os.path.basename(path).split(".")[0]

    pkg = deflib.property.Properties()
    pkg.prj = engine.project.name
    pkg.name = kw.get("name", __get_name__())
    pkg.machine = kw.get("machine", engine.opt.target_machine)
    pkg.cpu = kw.get("cpu", engine.opt.target_cpu)
    pkg.version = kw.get("version", deflib.pkg.Version(engine.project.version))
    pkg.build_config = engine.opt.build_config

    pkg.requires = kw.get("requires", [])
    
    pkg.checksum = deflib.pkg.checksum(path)
    pkg.time = str(datetime.datetime.now()) 
    pkg.whois = pwd.getpwuid(os.getuid())[0] + "@" + platform.uname()[1]  

    try:
        info = engine.repo.lookup(pkg.machine, pkg.cpu, pkg.prj, pkg.version, pkg.build_config, pkg.name)
        if info.checksum == pkg.checksum and info.requires == pkg.requires:
            deflib.logtool("repo", "Uptodate %s" % (info.name), False)
            return pkg
    except:
        pass

    deflib.logtool("repo", "Uploding %s" % (pkg.name), False)
    engine.repo.push(pkg, path)
    
    return pkg
