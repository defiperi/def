'''
Created on Sep 12, 2012

@author: bulentk
'''
import datetime

class Tick(object):
    def __init__(self, start):
        self.span = datetime.datetime.utcnow()
        if start:
            self.span = self.span - start.span
        
    def __str__(self):
        days = int(self.span.days)
        hours = self.span.seconds // 3600
        minutes = (self.span.seconds - hours * 3600) // 60
        seconds = self.span.seconds - hours * 3600 - minutes * 60 + float(self.span.microseconds) / 1000 / 1000
        rslt = ""
        if days:
            rslt += "%dd" % days
        if days or hours:
            rslt += "%dh" % hours
        if days or hours or minutes:
            rslt += "%dm" % minutes
        return "%s%.3fs" % (rslt, seconds)
