
from deflib import tick
from deflib import logger
from deflib import property
from deflib import file
from deflib import sig
from deflib import task
from deflib import pkg
from deflib import repo

import imp
import os
import engine
import platform

current_machine = "pc" + "-" + platform.system().lower().split('-')[0]
current_machine_cpu = platform.processor()
if not current_machine_cpu: 
    current_machine_cpu = platform.machine()
default_build_config = "release"

def tick_count(start = None):
    return tick.Tick(start)

def load_module(filepath, mdl = None):
    if not os.path.exists(filepath):
        raise Exception("module file does not exist !!! <" + filepath + ">")
    if not mdl:
        mdl = imp.new_module(filepath)
    f = None
    try:
        f = open(filepath, "rU")
        src = f.read()
        exec(compile(src, filepath, 'exec'), mdl.__dict__)
    finally:
        if f:
            f.close()
    return mdl

def logtool(tool, logstr, bold = False):
    ccc = "%s.%s" % (engine.cwc, tool)
    if len(ccc) > 16:
        tcc = ""
        i = 0
        for c in ccc:
            tcc += c
            if ++i > 15:
                break
        ccc = tcc
    space = " "*(16 - len(ccc))
    cclog = logger.blue(ccc)
    pathlog = logstr
    if bold:
        cclog = logger.bold(cclog) 
    logger.log("%s %s %s" % (space, cclog, pathlog))
