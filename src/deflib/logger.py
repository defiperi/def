'''
Created on Sep 12, 2012

@author: bulentk
'''

import sys

def err(s):
    lst = s.split("\n")
#    sys.stderr.write("\x1b[31m")
    for l in lst:
        if len(l):
            sys.stderr.write("%s\n" % (l))
            sys.stderr.flush()
#    sys.stderr.write("\x1b[0m")

def wrn(s):
    lst = s.split("\n")
#    sys.stdout.write("\x1b[33m")
    for l in lst:
        if len(l):
            sys.stdout.write("%s\n" % (l))
            sys.stderr.flush()
#    sys.stdout.write("\x1b[0m")

def log(s):
    sys.stdout.write(s + "\n")
    sys.stderr.flush()

def green(s):
#    return "\x1b[32m%s\x1b[0m" % (s)
    return s

def red(s):
#    return "\x1b[01;31m%s\x1b[0m" % (s)
    return s

def yellow(s):
#    return "\x1b[33m%s\x1b[0m" % (s)
    return s

def bold(s):
#    return "\x1b[01;1m%s\x1b[0m" % (s)
    return s

def blue(s):
#    return "\x1b[34m%s\x1b[0m" % (s)
    return s
