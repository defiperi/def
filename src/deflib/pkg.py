'''
Created on Nov 29, 2012

@author: bulentk
'''
import tarfile
import os
import deflib
import shutil
import md5

ext = "tar.bz2"


class Version(deflib.property.Properties):
    def __init__(self, version):
        super(Version, self).__init__()
        self.major = -1
        self.minor = -1
        self.revision = -1
        
        if isinstance(version, str):
            if len(version) > 0:
                v = str.split(version, ".")
                l = len(v)
                if l == 3:
                    self.major = int(v[0])
                    self.minor = int(v[1])
                    self.revision = int(v[2])
                elif l == 2:
                    self.major = int(v[0])
                    self.minor = int(v[1])
                    self.revision = -1
                elif l == 1: 
                    self.major = int(v[0])
                    self.minor = -1
                    self.revision = -1
        else:
            self.update(version)
            
        self.addimport("deflib")

    def __str__(self):
        if self.major == -1:
            return "x.x.x"
        elif self.minor == -1:
            return "%d.x.x" % (self.major)
        elif self.revision == -1:
            return "%d.%d.x" % (self.major, self.minor)
        return "%d.%d.%d" % (self.major, self.minor, self.revision)

    def __repr__(self, *args, **kwargs):
        s = ""
        if self.major != -1:
            if self.minor == -1:
                s = "%d" % (self.major)
            elif self.revision == -1:
                s = "%d.%d" % (self.major, self.minor)
            else:
                s = "%d.%d.%d" % (self.major, self.minor, self.revision)
        
        return "deflib.pkg.Version(\'%s\')" % (s)

    def __cmp__(self, that):
        t = that if isinstance(that, Version) else Version(that)
        if self.major == t.major:
            if self.minor == t.minor:
                if self.revision == t.revision:
                    return 0
                elif self.revision < t.revision:
                    return -1
                return 1
            elif self.major < t.major:
                return -1
            return 1
        elif self.major < t.major:
            return -1
        return 1


class File():
    def __init__(self, s, d):
        self.src = s
        self.dst = d

def create(filepath, files):
#     deflib.log("create", os.path.basename(filepath))
    tar = tarfile.open(filepath, 'w:bz2')
    for f in files:
        tar.add(f.src, arcname=f.dst, recursive=False)
    tar.close()

def extract(pkg_file, destination):
    tar = tarfile.open(pkg_file, 'r:bz2')
    tar.extractall(destination)
    lst = tar.getnames()
    tar.close()
    
    return lst

def checksum(pkg, block_size=2**16):
    f = None
    try:
        f = open(pkg, 'r')
        m = md5.new()
        while True:
            data = f.read(block_size)
            if not data:
                break
            m.update(data)
        return m.hexdigest()
    finally:
        if f:
            f.close()


def install(pkg, install_dir):
    d = os.path.join(install_dir, ".pkg_info")
#     key = "%s_%s" % (pkg.name, pkg.version)

    f = os.path.join(d, "%s_%s_%s_%s" % (pkg.prj, pkg.version, pkg.build_config, pkg.name))
    if os.path.exists(d):
        if os.path.exists(f):
            exist = deflib.property.from_file(f)
            if exist:
                e0 = deflib.property.Properties(exist)
                del e0.files
                if pkg == e0:
                    return 
                else:
                    uninstall(exist, install_dir)
    else:
        os.makedirs(d)

    deflib.logtool("pkgfs", "Installing %s (%s)" % (pkg.name, pkg.version), False)
    files = extract(pkg.path, install_dir)
    prop = deflib.property.Properties(pkg)
    prop.files = files
    deflib.property.to_file(f, prop)


def uninstall(pkg, rootdir):
    deflib.logtool("pkgfs", "Uninstalling %s (%s)" % (pkg.name, pkg.version), False)
    for f in pkg.files:
        node = os.path.join(rootdir, f)
        try:
            if os.path.exists(node):
                if os.path.isdir(node):
                    shutil.rmtree(node)
                else:
                    os.remove(node)
        except:
            pass
        
class PkgNotFoundException(Exception):
    def __init__(self, name, version):
        self.name = name
        self.version = version

    def __str__(self):
        return "Package does not exist !!! name:%s (%s)" % (self.name, self.version)        

