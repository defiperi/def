'''
lib.sig

created on: Aug 17, 2012
author    : bulent.kopuklu
email     : bulent.kopuklu@gmx.com.tr

Copyright (C) 2010 by Nekton
Please read the license agreement in license.txt
'''

import os
import md5


def generate(path):
    sig = None
    try:
        st = os.stat(path)
        m = md5.new()
        m.update(str(st.st_mtime))
        m.update(str(st.st_size))
        m.update(path)
        sig = m.hexdigest()
    except Exception:
        pass
    return sig

def from_file(filename):
    rslt = {}
    f = None
    try:
        f = open(filename, 'r')
        ln = f.readline()
        while ln:
            lst = ln.split("=")
            rslt[lst[0]] = str(lst[1]).rstrip("\n")
            ln = f.readline()
    except Exception:
        rslt.clear()
    finally:
        if f:
            f.close()
    return rslt

def to_file(filename, sigs):
    f = None
    try:
        if os.path.exists(filename):
            os.unlink(filename)
        f = open(filename, 'w')
        keys = sigs.keys()
        for k in keys:
            f.write("%s=%s\n" % (k, sigs[k]))
    finally:
        if f:
            f.close()
