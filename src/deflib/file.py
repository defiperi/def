'''
lib.file

created on: Aug 17, 2012
author    : bulent.kopuklu
email     : bulent.kopuklu@gmx.com.tr

Copyright (C) 2010 by Nekton
Please read the license agreement in license.txt
'''

import task, sig
import os
import deflib

class Node(object):
    def __init__(self, filepath):
        self.path = filepath
        self.signature = None
        
    def __str__(self):
        return self.path

    @property
    def name(self):
        return os.path.basename(self.path)

    @property
    def checksum(self):
        if not self.signature: 
            self.signature = sig.generate(self.path)
        return self.signature;

        
class Target(Node):
    def __init__(self, filepath):
        super(Target, self).__init__(filepath)
        self.deps = []

    def mkdep(self, n):
        if isinstance(n, Node):
            self.deps.append(n)
        elif isinstance(n, str):
            self.deps.append(Node(n))
        else:
            raise Exception("Invalid node type <" + type(n) + ">")
        
    def isuptodate(self, sigs):
        def __check(n):
            last = sigs.get(n.path)
            if not last or last != n.checksum:
                return False
            return True
        if not os.path.exists(self.path):
            return False
        fRslt = True
        for d in self.deps:
            if not __check(d):
                fRslt = False
                break
        return fRslt
      
        
    def create(self):
        raise Exception("pure-virtual method !!! <Target.create>")
    
class SymbolicLink(Target):
    def __init__(self, filepath, dst):
        super(SymbolicLink, self).__init__(filepath)
    def create(self):
        os.symlink(self.deps[0].name, self.path)

def symboliclink_creator(filepath, dst):
    return deflib.file.Generator("lnk", SymbolicLink(filepath, dst))

class Generator(task.Object):
    def __init__(self, name, target):
        super(Generator, self).__init__()
        self.name = name
        self.target = target
    
    def run(self):
        p, n = os.path.split(self.target.path)
        if not os.path.exists(p):
            try:
                os.makedirs(p)
            except:
                pass

        sigs = sig.from_file(os.path.join(p, "." + n))
        if not self.target.isuptodate(sigs):
            deflib.logtool(self.name, "Creating " + self.target.name)

            if os.path.exists(self.target.path):
                os.remove(self.target.path)
            
            self.target.create()
            
            sigs.clear()
            for d in self.target.deps:
                if not sigs.has_key(d.path):  
                    sigs[d.path] = d.checksum
            sig.to_file(os.path.join(p, "." + n), sigs)
        else:
            deflib.logtool(self.name, "Uptodate " + self.target.name)
    
    def mkdep(self, child):
        if isinstance(child, Generator):
            self.target.mkdep(child.target)
            self.childs.append(child)
        else:
            self.target.mkdep(child)

def mkdep(parent, child):
    if isinstance(child, (list, tuple)):
        for o in child:
            mkdep(parent, o)
    else:
        parent.mkdep(child)
