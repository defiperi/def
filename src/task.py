'''
Created on Sep 12, 2012

@author: bulentk
'''

import engine
import multiprocessing
import threading
import Queue
import sys
import traceback

class Worker(threading.Thread):
    def __init__(self, queue):
        super(Worker, self).__init__()
        self.queue = queue
    
    def run(self):
        while True:
            t = self.queue.get()
            try:
                t.run()
            except Exception, e:
#                 traceback.print_exc(file=sys.stderr)
                t.err = e
            self.queue.task_done()


class Manager(object):
    def __init__(self):
        self.queu = Queue.Queue()
        self.workers = []
        for i in range(0, multiprocessing.cpu_count()):
            t = Worker(self.queu)
            t.setDaemon(True)
            t.setName("Worker-%d" % i)
            t.start()
            self.workers.append(t)
    
    def execute(self, t):
        self.queu.put(t)
        
    def wait(self):
        self.queu.join()

def execute(tasks):
    def __err_check__(lst):
        for t in lst:
            if len(t.childs) > 0:
                __err_check__(t.childs)

            if t.err:
                raise t.err
            
    def __child__(lst):
        for t in lst:
            if len(t.childs) > 0:
                execute(t)
            else:
                engine.tm.execute(t)

        engine.tm.wait()
        __err_check__(lst)
                

    works = tasks if isinstance(tasks, (list, tuple)) else [tasks]
    for t in works:
        if len(t.childs) > 0:
            __child__(t.childs)

        engine.tm.execute(t)

    engine.tm.wait()
    __err_check__(works)
