'''
Created on Oct 10, 2012

@author: huseyinbiyik
'''

import deflib
import httplib
from ssl import SSLError
from socket import error as socketerror
import os

def mount(self):
    if not (os.path.exists(self.ssl_cert) or os.path.exists(self.ssl_key)):
        self.ssl_cert=None
        self.ssl_key=None
        deflib.logtool("repo.https","SSL key/certificate pair does not exist, trying to connect server without certificate")
    try:
        self.connection =  httplib.HTTPSConnection(self.url.netloc,cert_file=self.ssl_cert,key_file=self.ssl_key)
        self.connection.connect()
    except (httplib.HTTPException,SSLError,socketerror) , e:
        deflib.logtool("repo.https",e)
        raise e
    
    self.connection.request('HEAD', self.url.path)
    if not self.connection.getresponse().status in [200,204]:
        raise Exception("Invalid repository address !!!" + self.url.path)
    
    self.transaction=transaction
    
def unmount(self):
    self.connection.close()
    
def transaction(connection,req,valid=[200,204],exception=True):
    connection.request(*req)
    result = connection.getresponse()
    status=result.status
    if not status in valid:
        if exception:
            raise Exception("Error: Server returned %d for; %s : %s" % (status,req[0],req[1]))
        else:
            return None
    else:
        return result
    
def merge_uris(uris):
    uris= [uri.replace("/","").replace("\\","") for uri in uris ]
    return "/".join(uris)
    

def lookup(self, machine, cpu, name, version = None):
    if not version:
        version=transaction(self.connection,("GET",merge_uris([self.url.path,machine, cpu, name,"current"]))).read()
    filepath = merge_uris([self.url.path,machine, cpu, name,version])
        
    try: 
        prop = deflib.property.from_str(transaction(self.connection,("GET",filepath+"/properties")).read()) 
        prop.path = filepath+"/pkg.tar.bz2"
    except:
        raise deflib.pkg.PkgNotFoundException(name, version)
    
    return prop

def pull(self, pkg, target):
    try:
        f=open(target,"w")
        f.write(transaction(self.connection,("GET",pkg.path)).read())
        f.close()
    except IOError, e:
        #    if e.errno == 13:
        #        cmds = ["sudo", "cp", filepath, target]
        raise e
     
def push(self, pkg, source):
    rootpath = merge_uris([self.url.path,pkg.machine, pkg.cpu, pkg.name])
    pkgpath = merge_uris([self.url.path,pkg.machine, pkg.cpu, pkg.name,pkg.version])
    
    f = open(source,"r")
    body_content=f.read()
    f.close() 
    transaction(self.connection,('PUT', pkgpath+"/pkg.tar.bz2", body_content))
    transaction(self.connection,('PUT', pkgpath+"/properties", deflib.property.to_str(pkg)))
    
    if transaction(self.connection,('HEAD',rootpath+"/current"),exception=False):
        ph0, ph1, pl = pkg.version.split(".")
        pv = (int(ph0) << 24) | (int(ph1) << 16) | int(pl)
        cr_version=transaction(self.connection,("GET",rootpath+"/current")).read()
        cr_path=merge_uris([pkg.machine, pkg.cpu, pkg.name,cr_version])
        crntprop = deflib.property.from_str(transaction(self.connection,("GET",cr_path+"/properties")).read()) 
        
        ch0, ch1, cl = crntprop.version.split(".")
        cv = (int(ch0) << 24) | (int(ch1) << 16) | int(cl)
        if pv > cv:
            transaction(self.connection,("DELETE",rootpath+"/current"))
            transaction(self.coonection,("PUT",rootpath+"/current",pkg.version))
    else:
        transaction(self.connection,("PUT",rootpath+"/current",pkg.version))
