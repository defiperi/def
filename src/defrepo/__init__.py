import urlparse
import engine

class Repository(object):
    url = None
    def __init__(self, settings):
        self.url = urlparse.urlparse(engine.settings.repository)
        self.ssl_cert=settings.ssl_cert
        self.ssl_key=settings.ssl_key

    class __callable__(object):
        def __init__(self, impl, this):
            self.this = this
            self.impl = impl
        def __call__(self, *k, **kw):
            return self.impl(self.this, *k, **kw)
            
def create(settings):
    p = urlparse.urlparse(settings.repository)
    scheme = p.scheme
    if not scheme:
        scheme = "file"
        
    impl = __import__(scheme)

    repo = Repository(settings)
    for f in ["pull", "push", "lookup","mount","unmount"]:
        setattr(repo, f, Repository.__callable__(getattr(impl, f), repo))
    repo.mount()
    return repo

