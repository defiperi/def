'''
Created on Oct 10, 2012

@author: bulentk
'''

import os
import shutil
import subprocess
import deflib

def mount(self):
    # TODO check !!! is remote fs
    pass

def unmount(self):
    pass

def lookup(self, machine, cpu, prj, version, build_config, name):
    if not os.path.isdir(self.url.path):
        raise Exception("Invalid repository address !!!" + self.url.path)

    filepath = os.path.join(self.url.path, machine, cpu, prj)
    if version.major != -1:
        if version.minor != -1:
            if version.revision != -1:
                filepath = os.path.join(filepath, str(version.major), str(version.minor), str(version.revision))
            else:
                filepath = os.path.join(filepath, str(version.major), str(version.minor), "latest")
        else:
            filepath = os.path.join(filepath, str(version.major), "latest")
    else:
        filepath = os.path.join(filepath, "latest")
        
    filepath = os.path.join(filepath, build_config, name)

    try: 
        prop = deflib.property.from_file(os.path.join(filepath , "properties"))
        prop.path = os.path.join(filepath, "pkg.tar.bz2")
    except:
        raise deflib.pkg.PkgNotFoundException(name, version)

    return prop

def pull(self, pkginf, target):
    if not os.path.isdir(self.url.path):
        raise Exception("Invalid repository address !!!" + self.url.path)

    filepath = pkginf.path        
    if not os.path.exists(filepath):
        raise Exception("file not found")

    try:
        shutil.copy2(filepath, target)
    except IOError, e:
        if e.errno == 13:
            cmds = ["sudo", "cp", filepath, target]
            p = subprocess.Popen(cmds)
            p.wait()
        else:
            raise
            
def push(self, pkginf, source):
    if not os.path.isdir(self.url.path):
        raise Exception("Invalid repository address !!!" + self.url.path)

    repodir = os.path.join(self.url.path, pkginf.machine, pkginf.cpu, pkginf.prj)
    target = os.path.join(repodir, str(pkginf.version.major), str(pkginf.version.minor), str(pkginf.version.revision), pkginf.build_config, pkginf.name)
    
    try:
        os.makedirs(target)
    except:
        pass

    shutil.copy2(source, os.path.join(target, "pkg.tar.bz2"))
    deflib.property.to_file(os.path.join(target, "properties"), pkginf)

    major = str(pkginf.version.major)
    minor = str(pkginf.version.minor)
    revision = str(pkginf.version.revision)
    
    link = os.path.join(repodir, major, minor, "latest")
    if os.path.exists(link):
        exist = int(os.readlink(link))
        if exist < pkginf.version.revision:
            os.remove(link)
            os.symlink(revision, link)
    else:
        os.symlink(revision, link)
    
    link = os.path.join(repodir, major, "latest")
    if os.path.exists(link):
        exist = int(str.split(os.readlink(link), "/")[0])
        if exist < pkginf.version.minor:
            os.remove(link)
            os.symlink(os.path.join(minor, "latest"), link)
    else:
        os.symlink(os.path.join(minor, "latest"), link)
    
    link = os.path.join(repodir, "latest")
    if os.path.exists(link):
        exist = int(str.split(os.readlink(link), "/")[0])
        if exist < pkginf.version.major:
            os.remove(link)
            os.symlink(os.path.join(major, "latest"), link)
    else:
        os.symlink(os.path.join(major, "latest"), link)
