'''
Created on Oct 10, 2012

@author: bulentk
'''
from https import *
import deflib

def mount(self):
    try:
        self.connection =  httplib.HTTPConnection(self.url.netloc)
        self.connection.connect()
    except (httplib.HTTPException,SSLError,socketerror) , e:
        deflib.logtool("repo.http",e)
        raise e
    
    self.connection.request('HEAD', self.url.path)
    if not self.connection.getresponse().status in [200,204]:
        raise Exception("Invalid repository address !!!" + self.url.path)
    
    self.transaction=transaction