'''
Created on Sep 12, 2012

@author: bulentk
'''
from defcmd import base
import shutil
import engine
import os
import deflib

class Context(base.Context1):
    def __init__(self):
        super(Context, self).__init__("clean")

def default(cntx):
    pfile = engine.property_file()
    if os.path.exists(pfile):
        deflib.logtool("rm", "properties files")
        os.remove(pfile)
        shutil.copy2(pfile + ".first", pfile)

    if os.path.exists(cntx.output):
        deflib.logtool("rm", "output files")
        shutil.rmtree(cntx.output)
    
    engine.reset_globals()
    if os.path.exists(pfile):
        engine.load_env0(engine.property_file())
    

