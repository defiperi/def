'''
Created on Sep 12, 2012

@author: bulentk
'''

import deflib
import defext
from defcmd import base
import engine, prj
import os


def ctor(cntx):

    engine.reset_globals()
    engine.env = deflib.property.Properties()
    lstall = []
    for req in engine.project.requires:
#         print "request:", req
#         p = deflib.repo.lookup(req.prj, req.version, req.name)
#         print "properties:", p, "\n"
        pkgs = deflib.repo.downloadall(
                           engine.project.name, 
                           req
                           )
         
        req.version = pkgs[0].version
        for p in pkgs:
            lstall.append(p)
              
    cntx.project.__add_deppkgs__(lstall)
    for p in cntx.project.alldeppkgs:
        deflib.pkg.install(p, cntx.pkgfs)

def default(cntx):
    pass

def dtor(cntx):
    try:
        os.makedirs(cntx.output)
    except:
        pass
    


class Context(base.Context1):
    def __init__(self):
        super(Context, self).__init__("configure")

    def set_env(self, e, name = None):
        engine.env = e
        
    def create_env(self, parent = None):
        return deflib.property.Properties(parent)
    
    def import_tool(self, name):
        if defext.installed.has_key(name):
            return
        mdl = defext.install(name)
        init = getattr(mdl, "initialize", None)
        if init:                
            init(self)
        base.set_ext(self, name)
        
    def add_dep(self, prj_name, version = "", pkg=None, build_config="", scope="product", url=None):
        p = self.project.add_dep(prj_name, pkg, version, build_config, scope, url)
        pkg = deflib.repo.download(self.project.name, p)
        p.version = pkg.version

        self.project.__add_deppkgs__([pkg])
        

        deflib.pkg.install(pkg, self.pkgfs)

    def install(self, prj_name, version = "", pkg=None):
        inf = deflib.repo.lookup(prj_name, deflib.pkg.Version(version), pkg)
        inf.path = deflib.repo.download(self.project.name, inf)
        deflib.pkg.install(inf, self.pkgfs)
#     def prepare_requires(self):
#         lstall = []
#         for req in engine.project.requires:
#             pkgs = deflib.repo.downloadall(engine.project.name, req.name, req.version)
#             if not req.version:
#                 req.version = pkgs[0].version
#             for p in pkgs:
#                 lstall.append(p)
#         self.install_requires(lstall)
#         
#     def install_requires(self, lstall):
#         lst = clear_duplicate(lstall)
#         for p in lst:
#             deflib.pkg.install(p, self.pkgfs)
