'''
Created on Sep 12, 2012

@author: bulentk
'''
from defcmd import base
import engine
import deflib
import os

def ctor(cntx):
    cntx.add_option("--target-machine", 
                   type="string", metavar="NAME", 
                   help="Name of target machine.", 
                   )
    
    cntx.add_option("--target-cpu", 
                   type="string", metavar="NAME", 
                   help="Name of target machine cpu" 
                   )
    
    cntx.add_option("--build-config", 
                   type="string", metavar="NAME", 
                   help="build variant" 
                   )
def default(cntx):
    pass

def dtor(cntx):
    folder = engine.meta_root()
    optfile = os.path.join(folder, "options")
    engine.opt = engine.parser.parse_args()[0]

    last = None
    try:
        last = deflib.property.from_file(optfile)
    except:
        pass
    print "last :", last
    if not engine.opt.target_machine:
        engine.opt.target_machine = last.target_machine if last != None else deflib.current_machine
    if not engine.opt.target_cpu:
        engine.opt.target_cpu = last.target_cpu if last != None else deflib.current_machine_cpu
    if not engine.opt.build_config:
        engine.opt.build_config = last.build_config if last != None else deflib.default_build_config
    p = deflib.property.Properties(engine.opt.__dict__)
    for it in p.__table__.items():
        if it[1] == None and last != None:
            p[it[0]] = last[it[0]]

    if not os.path.exists(folder):
        os.makedirs(folder)

    if last != p:    
        deflib.property.to_file(optfile, p)

class Context(base.Context0):
    def __init__(self):
        super(Context, self).__init__("options")
            
    def add_option(self, *k, **kw):
        if engine.parser.has_option(k[0]):
            return engine.parser.get_option(k[0])
        return engine.parser.add_option(*k, **kw)
    
    def add_option_group(self, *k, **kw):
        return engine.parser.add_option_group(*k, **kw)

    def get_option_group(self, opt_str):
        return engine.parser.get_option_group(opt_str)
