'''
Created on Sep 12, 2012

@author: bulentk
'''
import script
import os
import defext
import engine
import deflib
import subprocess
import sys

class Context0(object):
    def __init__(self, cmd_name):
        self.cmd_name = cmd_name
        self.cws = engine.cws
        lst = defext.installed.keys()
        for e in lst:
            set_ext(self, e) 
                    
    def subs(self, *k):
        owner = self.cws
        for m in k:
            path = os.path.join(owner.path, m)
            os.chdir(path)
            s = script.load(path, owner)
            script.execute(s, self)            
            os.chdir(owner.path)
    
    def defne(self, *k, **kw):
        args=["def"]
        for arg in k: 
            args.append(arg)

        p = subprocess.Popen(args,                              
                              stdout=sys.stdout, 
                              stderr=subprocess.PIPE,
                              cwd=kw.get("file", ".")
                              )
        if p.wait():
            raise Exception(stderr)
            

class Context1(Context0):
    def __init__(self, cmd_name):
        super(Context1, self).__init__(cmd_name)
        self.output = os.path.join(self.settings.output, self.opt.target_machine, self.opt.target_cpu, self.opt.build_config)
        
    @property
    def settings(self):
        return engine.settings
    
    @property
    def root(self):
        return engine.root
    
    @property
    def opt(self):
        return engine.opt
    
    @property
    def env(self):
        return engine.env
    
    @property
    def pkgfs(self):
        return os.path.join(engine.meta_folder(engine.pkg_filesystem()))
    
    @property
    def project(self):
        return engine.project
    
    def add_file_object(self, node, key = None):

        def __add__(n):
            if not isinstance(n, (str, deflib.file.Node)):
                raise Exception("Invalid file type !!! " + str(type(n)))
            filepath = n.path if isinstance(n, deflib.file.Node) else n
            name = key if key else os.path.basename(filepath)
            if engine.node.has_key(name):
                val = engine.node[name]
                if isinstance(val, list):
                    if not filepath in val:
                        val.append(filepath)
                else:
                    if val != filepath:
                        engine.node[name] = [val, filepath]
            else:
                engine.node[name] = filepath

        if isinstance(node, (list, tuple)):
            for n in node:
                __add__(n)
        else:
            __add__(node)
            
        
    def get_file_object(self, name):
        node = engine.node.get(name)
        if not node:
            raise Exception("File not found !!! name:<", name, ">")
        
        if not isinstance(node, list):
            return deflib.file.Node(node)
        
        lst = []
        for n in node:
            lst.append(deflib.file.Node(n))

        return lst
    
def set_ext(cntx, name):
    mdl = defext.get(name)
    if not mdl:
        return
    lst = None
    try:
        exports = "__" + cntx.cmd_name + "__"
        lst = getattr(mdl, exports)
    except AttributeError:
        pass

    if lst == None:
        try:
            exports = "__" + "all" + "__"
            lst = getattr(mdl, exports)
        except AttributeError:
            return
    
    methods = []
    for m in lst:
        methods.append(getattr(mdl, m))
    ext = defext.Extention(methods, cntx)
    setattr(cntx, name, ext)
