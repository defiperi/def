'''
Created on Sep 12, 2012

@author: bulentk
'''
from defcmd import base
import engine
import deflib
import os
import shutil

def ctor(cntx):
    pass

def default(cntx):
    if engine.repo:
        pkgs = cntx.get_allpkgs()
        for p in pkgs:
            cntx.publish(p.name, p.path, p.requires)
    else:
        deflib.logger.wrn("Could not find available repository !!!")
        deflib.logger.wrn("repository=URL >> " + os.path.expanduser('~') + os.path.sep + ".def" + os.path.sep + "settings")
            
class Package(object):
    def __init__(self, name, path, requires = []):
        self.name = name
        self.path = path
        self.requires = requires

class Context(base.Context1):
    def __init__(self):
        super(Context, self).__init__("publish")
        
    def get_pkg(self, name):
        info = engine.packages.get(name)
        return Package(name, info.path, info.requires)
    
    def get_allpkgs(self):
        lst = []
        for n, inf in engine.packages.items():
            lst.append(Package(n, inf.get("path"), inf.get("requires")))
        return lst

    def publish(self, name, path, requires=None):
        if not name:
            raise Exception("Invalid package !!!")
        if not path:
            pass
        if not os.path.exists(path):
            raise Exception("Package file not found !!! <" + path + ">")

        if not requires:
            requires = []
        
        inf = deflib.repo.upload(path, name=name, requires=requires)
        if inf:
            target = os.path.join(engine.settings.cache, inf.machine, inf.cpu)
            if not os.path.exists(target):
                os.makedirs(target)
            if not os.path.isdir(target):
                raise Exception("Invalid target. Target is not directory ..." + target)
            
#             file_name = "%s_%s_%s_%s.%s" % (inf.prj, inf.version, inf.build_config, inf.name, deflib.pkg.ext)
#             
#             target = os.path.join(target, file_name)
#             if not os.path.exists(target):
#                 try:
#                     shutil.copy2(path, target)
#                 except:
#                     pass
                
# class Uploader(deflib.task.Object):
#     def __init__(self, name, path, requires):
#         super(Uploader, self).__init__()
#         self.name = name
#         self.path
#     
#     def run(self):
#         deflib.repo.upload(self.pkg.name,
#                      path = self.pkg.path,
#                      requires = self.pkg.requires
#                      )
#         
#         cache = os.path.join(engine.settings.cache, engine.opt.target_machine, engine.opt.target_cpu)
#         if not os.path.exists(cache):
#             os.makedirs(cache)
#         if not os.path.isdir(cache):
#             raise Exception("Cache is not directory ..." + cache)
# #         name = self.pkg.name
# #         print name
# 
# #         target = "%s.%s" % (os.path.join(cache, self.pkg.name + "_" + inf.version), 
# #                             deflib.pkg.ext
# #                             )
#         
