'''
Created on Jan 2, 2015

@author: bulentk
'''

from defcmd import base
import defext
    
def default(cntx):
    pass

class Context(base.Context1):
    def __init__(self):
        super(Context, self).__init__("build")
        self.cmd_name = "test"

