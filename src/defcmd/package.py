'''
Created on Sep 12, 2012

@author: bulentk
'''

from defcmd import base
import engine
import os
import deflib
import task
import glob
from prj import Dependency

class Context(base.Context1):
    def __init__(self):
        super(Context, self).__init__("package")
    
    def pkg_object(self):
        return Object()

    def make_pkg(self, obj, name = None, requires=None):
        if not isinstance(obj, Object):
            raise Exception("Invalid package object !!! " + str(type(obj)))
        
        if not name:
            name = self.project.name

        pkg = Package(self.output, name, obj)
        task.execute(deflib.file.Generator("mkpkg", pkg))
        engine.packages[name] = {"path":pkg.path, "requires":resolve_requires(requires)}
        return pkg
    
    def make_devel(self, pkg, obj, requires=None):
        if not isinstance(pkg, Package):
            raise Exception("Invalid package object !!! " + str(type(pkg)))
        
        p = engine.packages.get(pkg.name, None)
        if not p:
            raise Exception("Package file does not registered  !!! pkg:" + pkg.name)
        path = p.get("path", None)
        if not path:
            raise Exception("Package file does not registered  !!! pkg:" + pkg.name)
        if path != pkg.path:
            raise Exception("Invalid package object !!! <path>")
        if not os.path.exists(path):
            raise Exception("Package file does not exist  !!! pkg:" + pkg.path)

        reqs = [pkg]
        if requires:
            if isinstance(requires, (list, tuple)):
                reqs.extend(requires)
            else:
                reqs.append(requires)
        
        return self.make_pkg(obj, pkg.name + "-devel", reqs)

def resolve_requires(requires):
    lst = []
    if requires:
        def __mkdeps(dep):
            def __validate(prj, pkg, version):
                if not isinstance(prj, str):
                    raise Exception("Invalid parameter value.")

                if not isinstance(pkg, str):
                    raise Exception("Invalid parameter value.")

                if not isinstance(version, str):
                    raise Exception("Invalid parameter value.")

                return (prj, pkg, deflib.pkg.Version(version))


            if isinstance(dep, list):
                for l in dep:
                    __mkdeps(l)
                return
            if isinstance(dep, Package):
                lst.append((engine.project.name, dep.name, deflib.pkg.Version(engine.project.version)))
            elif isinstance(dep, Dependency):
                lst.append((dep.prj, dep.name, dep.version))
            elif isinstance(dep, tuple) and len(dep) == 1:
                lst.append(__validate(dep[0], dep[0], ""))
            elif isinstance(dep, tuple) and len(dep) == 2:
                lst.append(__validate(dep[0], dep[1], ""))
            elif isinstance(dep, tuple) and len(dep) == 3:
                lst.append(__validate(dep[0], dep[1], dep[2]))
            else:
                raise Exception("Invalid param type. " + str(type(dep)))
        __mkdeps(requires)
    else:
        for r in engine.project.requires:
            if r.scope == "product":
                lst.append((r.prj, r.name, r.version))
    return lst
            

class Object(object):
    def __init__(self):
        self.files = []
    
    def add_file(self, source, basedir, chmode=None):
        if isinstance(source, (tuple, list)):
            for f in source: 
                self.add_file(f, basedir, chmode)
            return

        src = source
        if isinstance(source, deflib.file.Node):
            src = source.path
        
        if not isinstance(src, str):
            raise Exception("Invalid source file type !!! " + str(type(source)))

        files = glob.glob(src)
        if not files:
            raise Exception("Source file(s) does not exist ... <" + src + ">")
        
        def list_file(src, dst):
            lst = [ deflib.pkg.File(src, dst) ]
            if os.path.isdir(src) and not os.path.islink(src):
                for f in os.listdir(src):
                    lst.extend(list_file(os.path.join(src, f), os.path.join(dst, f)))
            return lst

        for f in files:
            self.files.extend(list_file(f, os.path.join(basedir, os.path.basename(f))))
        
class Package(deflib.file.Target):
    def __init__(self, outdir, name, obj):
        super(Package, self).__init__("%s.%s" % (os.path.join(outdir, name), deflib.pkg.ext))
        self.pkgname = name
        self.files = obj.files
        
        for f in self.files:
            self.mkdep(f.src)

    @property
    def name(self):
        return self.pkgname
                    
    def create(self):
        deflib.pkg.create(self.path, self.files)
