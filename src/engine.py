'''
Created on Sep 12, 2012

@author: bulentk
'''
import optparse
import os
import task
import script
import deflib
import defcmd
import defext
import prj
import defrepo
import platform
import shutil
import intcmd

system = None

# project = deflib.property.Properties()
# project.decleration = prj.create()
# project.env = deflib.property.Properties()


project = None
tm = None
settings = None
cws = None
cwc = "system"
opt = None
env = None
node = {}
packages = {}
repo = None

root = os.getcwd()
parser = optparse.OptionParser()

def reset_globals():
    global env
    node.clear()
    packages.clear()
    if None != env:
        del env
        env = None
    if None != defext.installed:
        defext.installed.clear()
    
    del project.alldeppkgs[:]
        
def meta_root():
    return os.path.join(settings.output, ".metadata") 

def meta_folder(root=None):
    return os.path.join(root if None != root else meta_root(), opt.target_machine, opt.target_cpu, opt.build_config) 

def property_file():
    return os.path.join(meta_root(), meta_folder(), "property")

def pkg_filesystem():
    return os.path.join(root, ".depfs")

def store_env():
    if not os.path.exists(os.path.dirname(property_file())):
        os.makedirs(os.path.dirname(property_file()))

    prop = deflib.property.Properties()
    
    prop.env = env
    prop.ext = defext.installed.keys()
    prop.node = node
    prop.pkg = packages
    
    deflib.property.to_file(property_file(), prop)

def copy_env():
    shutil.copy2(property_file(), property_file() + ".first")

first = True
def load_env():
    global first
    if not first:
        return 
    load_env0(property_file())
    first = False

def load_env0(pfile):
    global env
    
    try:
        prop = deflib.property.from_file(pfile)
        env = deflib.property.Properties(prop.env)
        for e in prop.ext:
            defext.install(e)
        node.update(prop.node)
        packages.update(prop.pkg)
    except:
        raise Exception("Please run configure !!!")

def init_settings():
    global settings

    dpath = os.path.join(os.path.expanduser('~'), ".config","def")
    if not os.path.exists(dpath):
        os.makedirs(dpath)
    
    def __defaults__():
        s = deflib.property.Properties()
        s.output = "target"
        s.machine_confs = [ os.path.join(dpath, "machine-confs") ]        
        s.cache = os.path.join(dpath, "cache")
        s.repository = "file://" + s.cache
        s.toolchains = os.path.join(dpath, "toolchains")
        s.ssl_key = os.path.join(dpath, "user.key")
        s.ssl_cert = os.path.join(dpath, "user.crt")
        return s
    
    settings = __defaults__()
    spath = os.path.join(dpath, "settings")

    if not os.path.exists(settings.cache):
        os.makedirs(settings.cache)
    fIsUptodate = False
    if os.path.exists(spath):
        pf = deflib.property.from_file(spath)
        settings.update(pf)
        if settings == pf:
            fIsUptodate = True

    if not fIsUptodate:
        deflib.property.to_file(spath, settings)
        
    settings.output = os.path.abspath(settings.output)

def init_machine():
    isok = False
    for m in settings.machine_confs:
        mpath = os.path.join(m, opt.target_machine)
        try:
            machine = deflib.load_module(mpath)
            if not opt.target_cpu in machine.toolchains.keys():opt.target_cpu = machine.cpu
            isok = True
            break
        except:
            pass

    if isok == False:
        if opt.target_machine == deflib.current_machine:
            machine = deflib.property.Properties()
            machine.cpu = deflib.current_machine_cpu
            machine.toolchains = {}
            machine.toolchains[machine.cpu] = {"package":"", "prefix":""}
            try:
                os.makedirs(settings.machine_confs[0])
            except:
                pass
            deflib.property.to_file(os.path.join(settings.machine_confs[0], opt.target_machine), machine)
        else:
            raise Exception("Target machine property file does not exist !!! <%s>" % mpath)
            
    setattr(settings, "machine", machine)

def init_project():
    prj.create(os.path.basename(os.getcwd()))
    
def init_task_manager():
    global tm
    tm = task.Manager()

def init_repository():
    global repo
    repo = defrepo.create(settings)
    if not repo:
        pass
    
def fix_args(args):
    if len(args) > 0:
        try:
            i = args.index("options")
            args.pop(i)
        except ValueError:
            pass
    else:
        args.append("build")
        
    return args

def launch():
    global cwc

    init_project()
    init_settings()
    init_repository()
    init_task_manager()
    
    s = script.load(os.getcwd(), None)

    options(s)

    args = parser.parse_args()[1]
    cmds = fix_args(args)
    if len(cmds) > 0:
        init_machine()
        
        for c in cmds:
            cwc = c 
            runner = intcmd.lists.get(c)
            if runner:
                runner(s)
            else:
                scripting(s, c)
        
def scripting(s, cmd_name):
    print opt.target_cpu
    heading("%s <%s-%s> <%s-%s-%s>" % 
            (cmd_name, project.name, project.version, 
             opt.target_cpu, opt.target_machine, opt.build_config)
            )
    if  cmd_name != "configure":# and cmd_name != "clean" and cmd_name != "cleanall":
        load_env()
    tick = deflib.tick_count()

    cmd = None         
    try:
        cmd = getattr(defcmd, cmd_name)
        cntx = getattr(cmd, "Context")()
    except AttributeError:
        cntx = defcmd.base.Context1(cmd_name)

    script.execute(s, cntx, 
                   ctor=getattr(cmd, "ctor", None) if cmd != None else None, 
                   default=getattr(cmd, "default", None) if cmd != None else None, 
                   dtor=getattr(cmd, "dtor", None) if cmd != None else None
                   )

    deflib.logger.log(deflib.logger.bold("%s completed ... %s" % 
                                         (cmd_name, deflib.tick_count(tick))))
    store_env()    
    if cmd_name == "configure":
        copy_env()

def options(s):
    cntx = defcmd.options.Context()
    script.execute(s, cntx, 
               ctor=defcmd.options.ctor, 
               default=defcmd.options.default, 
               dtor=defcmd.options.dtor
               )

def heading(tstr, lstr = "-"):
    line = lstr * ((76 - len(tstr))/2)
    deflib.logger.log(deflib.logger.bold("\n%s%s%s\n" % 
                             (line, tstr, line)
                             ))
