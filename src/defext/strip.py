'''
ext.strip

author    : huseyinbiyik
email     : huseyinbiyik@hotmail.com

'''
import subprocess
import gnucc
import os
import deflib

__build__ = ["call"]
__package__ = ["call"]

def initialize(cntx):
    gnucc.initialize(cntx)
    strip = cntx.env.gnucc_compiler["strip"]
    if not strip:
        raise Exception("strip not found in your system !!!")
    try:
        cmds = [strip, "--version"]
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.wait():
            raise Exception(stderr)
        v = stdout.split("\n")[0]
        deflib.logtool("strip", v) 
    except:
        raise Exception("strip not found in your system !!!")


def call(cntx, *k, **kw):
    if not hasattr(cntx.env, "gnucc_compiler"):
        gnucc.initialize(cntx)

    strip = cntx.env.gnucc_compiler["strip"]
    if not strip:
        raise Exception("strip not found in your system !!!")
    args=[strip]
    infile=k[-1]
    if isinstance(infile, deflib.file.Node): 
        infile=infile.path
    outfile=infile

    for key in kw:
        if key=="o":
            outfile=kw[key]
            if isinstance(outfile, deflib.file.Node): 
                outfile=outfile.path
            if not os.path.isabs(outfile) : 
                outfile=os.path.join(os.path.dirname(os.path.realpath(infile)),outfile)
            kw[key]=outfile
        pre="-" if len(key)==1 else "--"
        args.append(pre+str(key).replace("_","=")+kw[key])


    for arg in k[:-1]: 
        pre="-" if len(arg)==1 else "--"
        args.append(pre+arg)

    args.append(infile)
    
    return Task(args,infile), deflib.file.Node(outfile)

class Task(deflib.task.Object):
    def __init__(self, args,infile):
        super(Task, self).__init__()
        self.args = args
        self.infile = infile
     
    def run(self):
        p = subprocess.Popen(self.args,                              
                              stdout=None, 
                              stderr=subprocess.PIPE 
                              )
        stderr = p.communicate()[1]
        if p.wait():
            raise Exception(stderr)
        deflib.logtool("strip", os.path.basename(self.infile))
