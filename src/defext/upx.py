'''
ext.upx

author    : huseyinbiyik
email     : huseyinbiyik@hotmail.com

'''
import subprocess
import os
import deflib

__build__ = ["call"]
__package__ = ["call"]

def initialize(cntx):
    upx = "upx"
    #todo: handle apths and windows ext.  
    try:
        cmds = [upx, "--version"]
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.wait():
            raise Exception(stderr)
        v = stdout.split("\n")[0]
        deflib.logtool("upx", v) 
    except:
        raise Exception("upx not found in your system !!!")
        #todo : auto downlaod according to host arch.


def call(cntx, *k, **kw):
    args=["upx"]
    infile=k[-1]
    if isinstance(infile, deflib.file.Node): infile=infile.path
    outfile=infile

    for key in kw:
        if key=="o":
            outfile=kw[key]
            if isinstance(outfile, deflib.file.Node): outfile=outfile.path
            if not os.path.isabs(outfile) : outfile=os.path.join(os.path.dirname(os.path.realpath(infile)),outfile)
            kw[key]=outfile
            sep=""
        else:
            sep="="
        pre="-" if len(key)==1 else "--"
        args.append(pre+str(key).replace("_","-")+sep+str(kw[key]))


    for arg in k[:-1]: 
        pre="-" if len(arg)==1 else "--"
        args.append(pre+arg)

    args.append(infile)
    
    return Task(args,infile), deflib.file.Node(outfile)

class Task(deflib.task.Object):
    def __init__(self, args,infile):
        super(Task, self).__init__()
        self.args = args
        self.infile = infile
     
    def run(self):
        p = subprocess.Popen(self.args,                              
                              stdout=None, 
                              stderr=subprocess.PIPE 
                              )
        stderr = p.communicate()[1]
        if p.wait():
            if "AlreadyPackedException" or "No such file or directory" in stderr:
                deflib.logtool("upx",stderr)
            else:
                raise Exception(stderr)
