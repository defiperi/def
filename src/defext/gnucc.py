'''
Created on Sep 13, 2012

@author: bulentk
'''
import deflib
import subprocess
import os
import engine


def initialize(cntx):
    init_toolchains(cntx)
    init_dependencies(cntx)

    init_ccflags(cntx)
    init_ldflags(cntx)
    init_preprocessor(cntx)

def get_package(p):
    prj = ""
    pkg = ""
    version = ""
    
    if not p:
        return None

    if isinstance(p, str):
        prj = p
        pkg = p
    elif isinstance(p, tuple) and len(p) == 1:
        prj = p[0]
        pkg = p[0]
    elif isinstance(dep, tuple) and len(dep) == 2:
        prj = p[0]
        pkg = p[1]
    elif isinstance(dep, tuple) and len(dep) == 3:
        prj = p[0]
        pkg = p[1]
        version = p[2]
    else:
        raise Exception("Invalid toolchain package decleration !!! ")
        
    return (prj, pkg, deflib.pkg.Version(version))
    

def init_toolchains(cntx):
    cntx.env.gnucc_compiler = {"g++":"", "gcc":"", "ar":"", "as":"","strip":""}
    toolchain_path = ""
    bindir = ""
    prefix = ""
    t = cntx.settings.machine.toolchains[cntx.opt.target_cpu]

    cntx.env.build = deflib.current_machine_cpu + '-' + deflib.current_machine
    cntx.env.host = t.get("host", cntx.env.build)
    pkg = get_package(t.get("package"))
    if pkg:
        if engine.repo:
            pkg_obj = deflib.repo.lookup(
                                 pkg[0], pkg[2], pkg[1],
                                 "release", deflib.current_machine, deflib.current_machine_cpu
                                 )
            deflib.repo.download(engine.project.name + "(gnucc)", pkg_obj)
            toolchain_path = os.path.join(cntx.settings.toolchains, pkg_obj.name + "_" + str(pkg_obj.version))
            deflib.pkg.install(pkg_obj, toolchain_path)

            bindir = os.path.join(toolchain_path, t.get("bindir", "bin"))
            if not t.has_key("prefix"):
                prefix = cntx.env.host
                if prefix:
                    prefix += "-"
            else:
                prefix = t.get("prefix")
            
        else:
            deflib.logger.wrn("Could not find available repository !!!")
            deflib.logger.wrn("repository=URL >> " + os.path.expanduser('~') + os.path.sep + ".def" + os.path.sep + "settings")
            raise Exception("File not found <" + pkg + ">")  

    cntx.env.gnucc_compiler["path"] = toolchain_path
    cntx.env.gnucc_compiler["g++"] = os.path.join(bindir, prefix + "g++")
    cntx.env.gnucc_compiler["gcc"] = os.path.join(bindir, prefix + "gcc")
    cntx.env.gnucc_compiler["ar"] = os.path.join(bindir, prefix + "ar")
    cntx.env.gnucc_compiler["as"] = os.path.join(bindir, prefix + "as")
    cntx.env.gnucc_compiler["strip"] = os.path.join(bindir, prefix + "strip")
    

def init_dependencies(cntx):
    lstdevel = []
    
    def __isexist(inf, newname):
        for d in lstdevel:
            if (d.prj == inf.prj and d.version == inf.version and d.name == newname):
                return True
        for d in cntx.project.alldeppkgs:
            if (d.prj == inf.prj and d.version == inf.version and d.name == newname):
                return True
        return False
    
    for d in cntx.project.alldeppkgs:
        name = d.name
        index = name.rfind("-devel")
        if index > 0 and index == len(name) - len("-devel"):
            continue
        
        name += "-devel"
        if __isexist(d, name):
            continue
        
        try:
            inf = deflib.property.Properties()

            inf.name = name
            inf.prj = d.prj
            inf.version = d.version
            inf.build_config = engine.opt.build_config
            
            lstall = deflib.repo.downloadall("gnucc", inf)
            for a in lstall:
                lstdevel.append(a)
        except Exception, e:
            print e

    for p in lstdevel:
        deflib.pkg.install(p, cntx.pkgfs)

def init_ccflags(cntx):
    ccflags = ["-Wall","-fmessage-length=0"]
    if cntx.opt.build_config == "debug":
        ccflags.extend(["-O0", "-g3", "-ggdb"])
    elif cntx.opt.build_config == "release":
        ccflags.append("-O2")
    
    if hasattr(cntx.env, "gnucc_ccflags"):
        cntx.env.gnucc_ccflags.extend(ccflags)
    else:
        cntx.env.gnucc_ccflags = ccflags

def init_ldflags(cntx):
    ldflags = []
#     if cntx.opt.build_config == "release":
#        ldflags.append("-s")

    if hasattr(cntx.env, "gnucc_ldflags"):
        cntx.env.gnucc_ldflags.extend(ldflags)
    else:
        cntx.env.gnucc_ldflags = ldflags

def init_preprocessor(cntx):
    defines = [
           ("TARGET_MACHINE", cntx.opt.target_machine), 
           ("TARGET_CPU", cntx.opt.target_cpu),
           ("BUILD_CONFIG", cntx.opt.build_config)
           ]
    
    if hasattr(cntx.env, "gnucc_preprocessors"):
        cntx.env.gnucc_preprocessors.extend(defines)
    else:
        cntx.env.gnucc_preprocessors = defines

#shared or static
def mkl(*k):
    libs = []
    def __mkl(lib):
        if isinstance(lib, (list, tuple)):
            for l in lib:
                __mkl(l)
        else:
            libs.append("-l")
            libs.append(lib)
    for l in k:
        __mkl(l)
    return libs 

#static
def mkw(*k):
    libs = ["-Wl,--whole-archive"]
    def __mkw(lib):
        if isinstance(lib, (list, tuple)):
            for l in lib:
                __mkw(l)
        else:
            libs.append(lib)
    for l in k:
        __mkw(l)
    libs.append("-Wl,--no-whole-archive")
    return libs

#static
def mks(*k):
    libs = ["-Wl,-Bstatic"]
    def __mks(lib):
        if isinstance(lib, (list, tuple)):
            for l in lib:
                __mks(l)
        else:
            libs.append("-l")
            libs.append(lib)
    for l in k:
        __mks(l)
    libs.append("-Wl,-Bdynamic")
    return libs

def mklib(_dumy_, *k, **kw):
    m = kw.get("mode", "l")
    _mk = {"l":mkl, "w":mkw, "s":mks}.get(m, None)
    if not _mk:
        raise Exception("Undefine mode !!! <" + m + ">")
    return None, _mk(*k)


# sources, include_dirs, preprocessors, cc_flags 
def obj(cc, cntx, *k, **kw):
    sources = kw.get("sources")
    if not sources:
        raise Exception("source files does not exists !!!")
    if not isinstance(sources, (list, tuple)):
        sources = [sources]
    
    incdirs = []
    _incdirs = kw.get("include_dirs", []) 
    if isinstance(_incdirs, str):
        _incdirs = [_incdirs]
    if not isinstance(_incdirs, (list, tuple)):
        raise Exception("Unexpected include_dirs decleration type. expected<list or tuple> type:" + str(type(_incdirs)))
    incdirs.extend(_incdirs)
    
    if hasattr(cntx.env, "gnucc_incdirs"):
        incdirs.extend(_incdirs)

    incdirs.append(os.path.join(cntx.pkgfs, "include"))
    incdirs.append(os.path.join(cntx.pkgfs, "usr", "local", "include"))
    incdirs.append(os.path.join(cntx.pkgfs, "usr", "include"))

    preprocessors = []
    _prepro = kw.get("preprocessors", [])
    if isinstance(_prepro, str):
        _prepro = [_prepro]
    if not isinstance(_prepro, (list, tuple)):
        raise Exception("Unexpected preprocessors decleration type. expected<list or tuple> type:" + str(type(_prepro)))
    preprocessors.extend(_prepro)

    if hasattr(cntx.env, "gnucc_preprocessors"):
        preprocessors.extend(cntx.env.gnucc_preprocessors)

    ccflags = []
    _ccflags = kw.get("cc_flags", [])
    if isinstance(_ccflags, str):
        _ccflags = [_ccflags]
    if not isinstance(_ccflags, (list, tuple)):
        raise Exception("Unexpected cc_flags decleration type. expected<list or tuple> type:" + str(type(_ccflags)))
    ccflags.extend(_ccflags)
    if hasattr(cntx.env, "gnucc_ccflags"):
        ccflags.extend(cntx.env.gnucc_ccflags)

    tasks = []
    nodes = []

    deflib.logtool(get_toolname(cc), "Checking the dependency tree of the source file(s)")

    for s in sources:
        path = cntx.output + os.path.sep + "obj" + os.path.sep
        p, f = os.path.split(os.path.abspath(s))
        if 0 == p.find(cntx.root):
            p = p[len(cntx.root) + 1:]

        p = p.replace(os.path.sep, ".")
        if 0 < len(p):
            path += p + "."
        path += f + ".o"

        tsk = deflib.file.Generator(get_toolname(cc), Object(cc, path, s, incdirs, preprocessors, ccflags))
        deflib.file.mkdep(tsk, dep(cc, s, path, incdirs, preprocessors, ccflags))
        nodes.append(tsk.target)
        tasks.append(tsk)

    return (tasks, nodes)

# objects, sources, include_dirs, define_macros, cc_flags
def ar(cc, cntx, *k, **kw): 
    name = k[0] if len(k) > 0 else ""
    if not name:
        raise Exception("target name not found !!!")
    outdir = kw.get("outdir", cntx.output)

    a = cntx.env.gnucc_compiler.get("ar")
    node = Archive(a, os.path.join(outdir, name))
    task = deflib.file.Generator(get_toolname(a), node)

    set_objects(task, node, cc, cntx, **kw)
    set_sources(task, node, cc, cntx, **kw)
        
    return (task, node)

# ld_flags, libraries, library_dirs, statics, objects, sources, include_dirs, define_macros, cc_flags
def so(cc, cntx, *k, **kw):
    
    name = k[0] if len(k) > 0 else ""
    if not name:
        raise Exception("target name not found !!!")
    outdir = kw.get("outdir", cntx.output)
    
    orig_name = name
    
    soname = None
    version = kw.get("version", None)
    if version:
        soname = name + "." + version.split(".")[0]
        name = "%s.%s" % (name, version) 

        
#     for s in sources:
#         path = cntx.output + os.path.sep + "obj" + os.path.sep
#         p, f = os.path.split(os.path.abspath(s))
#         if 0 == p.find(cntx.root):
#             p = p[len(cntx.root) + 1:]
# 
#         p = p.replace(os.path.sep, ".")
#         if 0 < len(p):
#             path += p + "."
#         path += f + ".o"
# 
#         tsk = deflib.file.Generator(get_toolname(cc), Object(cc, path, s, incdirs, preprocessors, ccflags))
#         deflib.file.mkdep(tsk, dep(cc, s, path, incdirs, preprocessors, ccflags))
#         nodes.append(tsk.target)
#         tasks.append(tsk)

    so_file = Shared(cc, os.path.join(outdir, name), soname)
    so_task = binary(so_file, cc, cntx, **kw)

    if soname:
        nodes = [so_file]
        tasks = [so_task]
                
        name_link = deflib.file.symboliclink_creator(os.path.join(outdir, soname), name)
        nodes.append(name_link.target)
        tasks.append(name_link)
        
        deflib.file.mkdep(name_link, so_task)

        dev_link = deflib.file.symboliclink_creator(os.path.join(outdir, orig_name), name_link.target)
        nodes.append(dev_link.target)
        tasks.append(dev_link)

        deflib.file.mkdep(dev_link, name_link)
                
        return (dev_link, nodes)

    return (so_task, so_file)

# ld_flags, libraries, library_dirs, statics, objects, sources, include_dirs, define_macros, cc_flags
def exe(cc, cntx, *k, **kw):
    name = k[0] if len(k) > 0 else ""
    if not name:
        raise Exception("target name not found !!!")
    outdir = kw.get("outdir", cntx.output)
    
    exe = Executable(cc, os.path.join(outdir, name), kw.get("version"))
    task = binary(exe, cc, cntx, **kw)

    return (task, exe)


def binary(binfile, cc, cntx, **kw):
    generator = deflib.file.Generator(get_toolname(cc), binfile)

    flags = kw.get("ld_flags", [])
    if not isinstance(flags, (list, tuple)):
        _flags = [flags]
    if hasattr(cntx.env, "gnucc_ldflags"):
        binfile.flags.extend(cntx.env.gnucc_ldflags)
    binfile.flags.extend(flags)

    libdirs = kw.get("library_dirs", [])
    if not isinstance(libdirs, (list, tuple)):
        libdirs = [libdirs]
    
    binfile.libdirs.extend(libdirs)
    if hasattr(cntx.env, "gnucc_libdirs"):
        binfile.libdirs.extend(cntx.env.gnucc_libdirs)
    binfile.libdirs.append(os.path.join(cntx.pkgfs, "lib"))
    binfile.libdirs.append(os.path.join(cntx.pkgfs, "usr", "lib"))
    binfile.libdirs.append(os.path.join(cntx.pkgfs, "usr", "local", "lib"))    

    set_libraries(generator, binfile, cc, cntx, **kw)    
    set_objects(generator, binfile, cc, cntx, **kw)
    set_sources(generator, binfile, cc, cntx, **kw)

    return generator

def dep_flags(name, values):
    def read():
        f = None
        try:
            f = open(name, 'r')
            return eval(f.read())
        finally:
            if f:
                f.close()
    def write():
        f = None
        try:
            f = open(name, 'w')
            f.write(str(values))
        finally:
            if f:
                f.close()
    if os.path.exists(name):
        val = read()
        if val != values:
            write()
    else:
        write()
        
    return name

def dep(cc, source, objs, incdirs, preprocessors, ccflags):
    p, n = os.path.split(objs)
    depfile = os.path.join(p, "." + n)
    
    if not os.path.exists(p):
        try:
            os.makedirs(p)
        except:
            pass

    ccfile = dep_flags(depfile+".ccflags", ccflags)
    prefile = dep_flags(depfile+".preprocessors", preprocessors)

    if os.path.exists(depfile):
        sigs = deflib.sig.from_file(depfile)
        if deflib.sig.generate(source) == sigs[source]:
            return sigs.keys()
    
    cmds = [cc]
    if incdirs:
        for i in incdirs:
            cmds.append("-I")
            cmds.append(i)

    if preprocessors:
        for d in preprocessors:
            cmds.append("-D")
            if isinstance(d, tuple):
                cmds.append("%s=%s" % (d[0], d[1]))
            else:
                cmds.append(d)

    if ccflags:
        cmds.extend(ccflags)
    
    cmds.append("-M")
    cmds.append(source)
    
    p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if p.wait():
        raise Exception(stderr)
    strout = str(stdout)
    strout = strout.replace(" \\\n", " ")
    lst = strout.split()
    lst.pop(0) # obj file name

    lst.append(ccfile)
    lst.append(prefile)

    return lst

def set_sources(generator, binfile, cc, cntx, **kw):
    if kw.has_key("sources"):
        tsk, node = obj(cc, cntx, **kw)
        binfile.objs.extend(node)
        deflib.file.mkdep(generator, tsk)
        
def set_objects(generator, binfile, cc, cntx, **kw):
    objs = kw.get("objects")
    if not objs:
        return
    if not isinstance(objs, (list, tuple)):
        objs = [objs]
    def __objects(lst):
        for o in lst:
            if isinstance(o, (list, tuple)):
                __objects(o)
            else:
                if not isinstance(o, deflib.file.Node):
                    o = deflib.file.Node(o)
                binfile.objs.append(o)
                deflib.file.mkdep(generator, o)

    __objects(objs)

def set_libraries(generator, binfile, cc, cntx, **kw):
    libs = kw.get("libraries")
    if not libs:
        return
    if not isinstance(libs, (list, tuple)):
        libs = [libs]
    def __libs(lst):
        for l in lst:
            if isinstance(l, (list, tuple)):
                __libs(l)
            else:
                if isinstance(l, deflib.file.Node):
                    deflib.file.mkdep(generator, l)
                    l = l.path
                elif isinstance(l, str):
                    if os.path.isabs(l):
                        deflib.file.mkdep(generator, l)
                binfile.libs.append(l)
    __libs(libs)
    
def get_toolname(cc):
    _cc = cc            
    i = cc.rfind("-")
    if i != -1:
        _cc = cc[i+1:]
    return _cc


class Object(deflib.file.Target):
    def __init__(self, cc, path, sourcefile, incdirs, preprocessors, flags):
        super(Object, self).__init__(path)
        self.cc = cc

        self.sourcefile = sourcefile
        self.incdirs = incdirs if incdirs else []
        self.preprocessors = preprocessors if preprocessors else []
        self.flags = flags if flags else []
        
    def create(self):
        cmds = [self.cc]

        for i in self.incdirs:
            cmds.append("-I%s" % i)
#            cmds.append(i)

        for d in self.preprocessors:
            s = d
            if isinstance(d, tuple):
                s = "%s=%s" % (d[0], d[1])
            cmds.append("-D%s" % s)
        
        for f in self.flags:
            cmds.append(f)

        cmds.append("-c")
        cmds.append(self.sourcefile) # source file

        cmds.append("-o")
        cmds.append(self.path)

        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stderr = p.communicate()[1]
        if p.wait():
            raise Exception(stderr)

class Archive(deflib.file.Target):
    def __init__(self, cc, path):
        super(Archive, self).__init__(path)
        self.cc = cc
        self.objs = []

    def create(self):
        def __extract_archive(arfile):
            p, f = os.path.split(arfile)
            out = os.path.join(p, "obj", f) 
            if not os.path.exists(out):
                os.makedirs(out)
            cmds = [self.cc, "x"]
            cmds.append(os.path.abspath(arfile))
            p = subprocess.Popen(cmds, cwd=out, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stderr = p.communicate()[1]
            if p.wait():
                raise Exception(stderr)
            
            lst = []
            for l in os.listdir(out):
                lst.append(os.path.join(out,l))
            return lst
        
        cmds = [self.cc, "rcus"]
        cmds.append(self.path)
        
        for o in self.objs:
            if isinstance(o, Archive):
                for l in __extract_archive(o.path):
                    cmds.append(l)
            else:
                if o.path.endswith(".a"):
                    for l in __extract_archive(o.path):
                        cmds.append(l)
                else:
                    cmds.append(o.path)
            
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stderr = p.communicate()[1]
        if p.wait():
            raise Exception(stderr)

class Binary(deflib.file.Target):
    def __init__(self, cc, path):
        super(Binary, self).__init__(path)
        self.cc = cc
        self.libdirs = [os.path.dirname(path)]
        self.objs = []
        self.libs = []
        self.flags = []
        
    def create(self):
        osenv = os.environ
        cmds = [self.cc]
        
        self.onBegin(cmds)

        cmds.append("-o")
        cmds.append(self.path)

        for f in self.flags:
            cmds.append(f)

        if self.libdirs:
            ldlibpath = osenv.get("LD_LIBRARY_PATH", "")
            for p in self.libdirs:
                cmds.append("-L%s" % p)
#                cmds.append(p)
                if ldlibpath:
                    ldlibpath += ":"
                ldlibpath += p
            osenv["LD_LIBRARY_PATH"] = ldlibpath
        for o in self.objs:
            cmds.append(o.path)
            
        for l in self.libs:
            cmds.append(l)
        
        self.onEnd(cmds)
        
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=osenv)
        stderr = p.communicate()[1]
        if p.wait():
            raise Exception(stderr)
    
    def onBegin(self, cmds):
        pass
    def onEnd(self, cmds):
        pass

class Shared(Binary):        
    def __init__(self, cc, path, soname = None):
        super(Shared, self).__init__(cc, path)
        self.soname = soname
        
#     def create(self):
#         Binary.create(self)
#         if self.soname:
#             sop = os.path.dirname(self.path)
#             last = self.name
#             crnt = self.soname
# 
#             while len(crnt):
#                 cpath = os.path.join(sop, crnt)
#                 deflib.log("ln", "%s -> %s" % (crnt, last))
#                 if os.path.exists(cpath):
#                     os.unlink(cpath)
#                 os.symlink(last, cpath)
#                 
#                 i = crnt.rfind(".")
#                 if i != -1:
#                     if not str.isdigit(crnt[i+1:]):
#                         break
#                 else:
#                     break
#                 last = crnt
#                 crnt = crnt[:i]
        
    def onBegin(self, cmds):
        cmds.append("-shared")
        if self.soname:
            cmds.append("-Wl,-soname,%s" % (self.soname))

class Executable(Binary):
    def __init__(self, cc, path, version):
        super(Executable, self).__init__(cc, path)
