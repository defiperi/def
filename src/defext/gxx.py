'''
created on: Aug 5, 2012
author    : bulent.kopuklu
email     : bulent.kopuklu@gmx.com.tr

Copyright (C) 2010 by Nekton
Please read the license agreement in license.txt
'''

import gnucc
import subprocess
import deflib

__configure__= []
__build__ = ["obj", "ar", "so", "exe", "mklib"]

def version(cntx):
    cxx = cntx.env.gnucc_compiler["g++"]
    if not cxx:
        raise Exception("g++ not found in your system !!!")
    try:
        cmds = [cxx, "--version"]
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.wait():
            raise Exception(stderr)
        return stdout.split("\n")[0]
    except:
        raise Exception("g++ not found in your system !!!")

def initialize(cntx):
    if not hasattr(cntx.env, "gnucc_compiler"):
        gnucc.initialize(cntx)
    deflib.logtool("g++", version(cntx)) 
    

def obj(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("g++")
    if not cc:
        raise Exception("compiler not found !!! <g++>")
    return gnucc.obj(cc, cntx, *k, **kw)

def ar(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("g++")
    if not cc:
        raise Exception("compiler not found !!! <g++>")
    return gnucc.ar(cc, cntx, *k, **kw)

def so(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("g++")
    if not cc:
        raise Exception("compiler not found !!! <g++>")
    return gnucc.so(cc, cntx, *k, **kw)
    
def exe(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("g++")
    if not cc:
        raise Exception("compiler not found !!! <g++>")
    return gnucc.exe(cc, cntx, *k, **kw)

mklib = gnucc.mklib 
