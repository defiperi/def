'''
Created on Oct 16, 2014

@author: bulentk
'''

import deflib
import os
import glob
import gnucc
import tarfile
import gxx
import platform
import datetime
import pwd

__configure__= []
__build__ = ["so", "bar"]

def initialize(cntx):
    cntx.import_tool("gxx");
#     env = cntx.create_env(cntx.env)
#     env.gnucc_ccflags.extend(["-fPIC", "-fexceptions", "-fvisibility=hidden"])
#     cntx.set_env(env)

def so(cntx, *k, **kw):
    name = k[0] if len(k) > 0 else "bundle.so"
    if not name:
        raise Exception("Missing target name. cntx.osgi.bar(\"name\")")
    if not isinstance(name, str):
        raise Exception("Unexpected target name type. expected:<str> type:" + str(type(name)))


    _ccflags = kw.get("cc_flags", [])
    if isinstance(_ccflags, str):
        _ccflags = [_ccflags]
    if not isinstance(_ccflags, list):
        raise Exception("Unexpected cc_flags decleration type. expected:<list> type:" + str(type(name))) 
    
    # TODO check -fnoexception & -fvisibility
    ccflags = ["-fPIC", "-fexceptions", "-fvisibility=hidden"]
    ccflags.extend(_ccflags)
    
    kw["cc_flags"] = ccflags
    
    task, target = gxx.so(cntx, name, **kw)
    task.target = BundleLibrary(target)

    return (task, task.target)
    
def bar(cntx, *k, **kw):
    name = k[0] if len(k) > 0 else ""
    if not name:
        raise Exception("Missing target name. cntx.osgi.bar(\"name\")")
    if not isinstance(name, str):
        raise Exception("Unexpected target name type. expected:<str> type:" + type(name))
    n, e = os.path.splitext(name)
    if e == ".bar":
        name = n 
    outdir = kw.get("outdir", cntx.output)
    if not isinstance(outdir, str):
        raise Exception("Unexpected outdir deleration type. expected:<str> type:" + type(outdir))

    if not kw.has_key("impl"):
        raise Exception("Missing impl decleration. cntx.osgi.bar(impl=cntx.osgi.so())")
    impl = kw.get("impl")
    if not impl:
        raise Exception("Unexpected impl decleration type. expected:<cntx.gxx.so()> type:None")
    if not isinstance(impl, BundleLibrary):
        raise Exception("Unexpected impl decleration type. expected:<cntx.gxx.so()> type:" + str(type(impl)))
    mf = manifest_dict(cntx, name, **kw)

    dict_name = "." + name + ".dict"
    mfpath = os.path.join(kw.get("outdir", cntx.output), dict_name)
    gnucc.dep_flags(mfpath, mf)

    files = extra_files(cntx, **kw)

    name = name + "-" + mf["Bundle-Version"] + ".bar"
    b = Bundle(os.path.join(outdir, name), impl, mfpath, files)
    
    b.mkdep(impl)
    b.mkdep(mfpath)
    
    if files:
        for f in files:
            b.mkdep(f.src)

    return (deflib.file.Generator("osgi.bar", b), b)

def manifest_dict(cntx, name, **kw):
    manifest = None
    if kw.has_key("manifest"):
        manifest = kw.get("manifest")
        if manifest == None:
            raise Exception("Unexpected manifest declaration type. expected:<dict> type:<None>")
        if not isinstance(manifest, dict):
            raise Exception("Unexpected manifest declaration type. expected:<dict> type:" + str(type(manifest)))

    mf = dict(manifest) if manifest != None else dict()
    def __check_and_setdef(key, default):
        value = mf.get(key, None)
        if value:
            if not isinstance(value,  str):
                raise Exception("Unexpected %s declaration into manifest. expected:<str> type:%s", (key,  str(type(value))))
        else:
            mf[key] = default
        
    __check_and_setdef("Bundle-Name", name)
    __check_and_setdef("Bundle-Version", cntx.project.version)
    __check_and_setdef("Bundle-SymbolicName", name)
    __check_and_setdef("Bundle-Vendor", "undefine")

    mf["Build-GCC"] = gxx.version(cntx) 
    mf["Build-By"] = pwd.getpwuid(os.getuid())[0] + "@" + platform.uname()[1]

    return mf

def extra_files(cntx, **kw):
    lst = kw.get("files", None)
    if not lst:
        return None
    if not isinstance(lst, list):
        raise Exception("Unexpected files decleration type. expected:<[(src_str, dst_str)]> " + str(type(lst)))

    rslt = []
    
    for f in lst:
        if not isinstance(lst, list):
            raise Exception("Unexpected files member decleration type. expected:<(src_str, dst_str)> " + str(type(f)))

        src = f[0]
        dst = f[1]
        if isinstance(src, deflib.file.Node):
            src = src.path
        if not isinstance(src, str):
            raise Exception("Unexpected files decleration type. expected:<str> type:" + str(type(src)))

        files = glob.glob(src)
        if not files:
            raise Exception("Source file(s) does not exist ... <" + src + ">")
    
        def list_file(src, dst):
            lst = [ deflib.pkg.File(src, dst) ]
            if os.path.isdir(src) and not os.path.islink(src):
                for f in os.listdir(src):
                    lst.extend(list_file(os.path.join(src, f), os.path.join(dst, f)))
            return lst

        for f in files:
            rslt.extend(list_file(f, os.path.join(dst, os.path.basename(f))))

    return rslt

class Bundle(deflib.file.Target):
    def __init__(self, path, impl, mf, files):
        super(Bundle, self).__init__(path)
        self.impl = impl
        self.mf = mf
        self.files = files

    def create(self):
        deflib.logtool("bar", self.name, True)
        tar = tarfile.open(self.path, 'w:gz')
        tar.add(self.impl.path, arcname="bundle.so", recursive=False)
        tar.add(self.manifest(), arcname="META-INF/MANIFEST.MF", recursive=False)
        if self.files:
            for f in self.files:
                tar.add(f.src, arcname=f.dst, recursive=False)
        tar.close()

    def readdict(self):
        f = None
        try:
            f = open(self.mf, "r")
            src = f.read()
            return eval(src)
        except Exception, e:
            raise e
        finally:
            if f:
                f.close()
        
    def manifest(self):
        mfd = self.readdict()
        
        f = None
        fn = self.mf + ".mf"
        try:
            f = open(fn, "w")
            for k in mfd.keys():
                f.write("%s=%s\n" % (k, mfd.get(k)))
            f.write("Build-Time=%s\n" % str(datetime.datetime.now()))
        finally:
            if f:
                f.close()
        return fn

class BundleLibrary(deflib.file.Target):
    def __init__(self, so):
        super(BundleLibrary, self).__init__(so.path)
        self.so = so
        
    def create(self):
        self.so.create()
