'''
ext.make

created on: Aug 25, 2012
author    : bulent.kopuklu
email     : bulent.kopuklu@gmx.com.tr

Copyright (C) 2010 by Nekton
Please read the license agreement in license.txt
'''
import subprocess
from deflib import task
import deflib
import sys
import os

__all__ = ["execute"]

def execute(cntx, *k, **kw):
    mkfile = kw.get("file", None)
    if mkfile:
        if not isinstance(mkfile, str):
            raise
        
    target = kw.get("target", None)
    if target:
        if not isinstance(target, str):
            raise
        
    args = kw.get("args", None)
    if args:
        if isinstance(args, str):
            args = [args]
        if not isinstance(args, (list, tuple)):
            raise
    
    env = None
    _env = kw.get("env", None)
    if _env:
        if not isinstance(_env, dict):
            raise 
        env = dict(os.environ)
        env.update(_env)
    
    return Task(mkfile, target, args, env), None

class Task(task.Object):
    def __init__(self, mkfile, target, args, env):
        super(Task, self).__init__()
        self.file = mkfile
        self.target = target
        self.args = args
        self.env = env
    
    def run(self):
        trc = ""
        cmds = ["make"]
        if self.file:
            cmds.append("-f")
            if isinstance(self.file, deflib.file.Node):
                cmds.append(self.file.path)
            else:
                cmds.append(self.file)  
            
        if self.args:
            if not isinstance(self.args, (list, tuple)):
                cmds.append(self.args)
            else:
                cmds.extend(self.args)
            
        if self.target:
            trc += " target:" + str(self.target)
            if isinstance(self.target, (list, tuple)):
                cmds.extend(self.target)
            else:
                cmds.append(self.target)
                
        deflib.logtool("make", trc) 

        sys.stdout.write("\x1b[2m")
        sys.stdout.flush()

        try:
            p = subprocess.Popen(cmds, 
                                 stdout=sys.stdout, 
                                 stderr=subprocess.PIPE, 
                                 env=self.env
                                 )

            stderr = p.communicate()[1]
            if p.wait():
                raise Exception(stderr)
        finally:
            sys.stdout.write("\x1b[0m")            
            sys.stdout.flush()

