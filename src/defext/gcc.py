'''
ext.gcc

created on: Aug 24, 2012
author    : bulent.kopuklu
email     : bulent.kopuklu@gmx.com.tr

Copyright (C) 2010 by Nekton
Please read the license agreement in license.txt
'''

import gnucc
import subprocess
import deflib

__configure__   = []
__build__       = ["obj", "ar", "so", "exe", "mklib"]


def initialize(cntx):
    if not hasattr(cntx.env, "gnucc_compiler"):
        gnucc.initialize(cntx)

    cc = cntx.env.gnucc_compiler["gcc"]
    if not cc:
        raise Exception("gcc not found in your system !!!")
    try:
        cmds = [cc, "--version"]
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.wait():
            raise Exception(stderr)
        v = stdout.split("\n")[0]
        deflib.logtool("gcc", v) 
    except:
        raise Exception("gcc not found in your system !!!")


def obj(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("gcc")
    if not cc:
        raise Exception("compiler not found !!! <gcc>")
    return gnucc.obj(cc, cntx, *k, **kw)

def ar(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("gcc")
    if not cc:
        raise Exception("compiler not found !!! <gcc>")
    return gnucc.ar(cc, cntx, *k, **kw)

def so(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("gcc")
    if not cc:
        raise Exception("compiler not found !!! <gcc>")
    return gnucc.so(cc, cntx, *k, **kw)
    
def exe(cntx, *k, **kw):
    cc = cntx.env.gnucc_compiler.get("gcc")
    if not cc:
        raise Exception("compiler not found !!! <gcc>")
    return gnucc.exe(cc, cntx, *k, **kw)

mklib = gnucc.mklib
