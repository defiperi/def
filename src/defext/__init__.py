import task
import os
import deflib

installed = {}

class Method(object):
    def __init__(self, mtd, cntx):
        self.mtd = mtd
        self.cntx = cntx
        
    def __call__(self, *k, **kw):
        tasks, rslt = self.mtd(self.cntx, *k, **kw)
        if tasks:
            task.execute(tasks)
        return rslt

class Extention(object):
    def __init__(self, metods, cntx):
        for m in metods:
            if m.__name__=="call":
                setattr(self.__class__, "__call__", Method(m, cntx))
                continue                
            setattr(self, m.__name__, Method(m, cntx))

def install(name):
    if installed.has_key(name):
        return installed[name]
    mdl = __import__(name)
    installed[name] = mdl
    return mdl

def get(name):
    if not installed.has_key(name):
        raise Exception("Please install module <" + name + ">")
    return installed[name]
