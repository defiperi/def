'''
ext.automake

created on: Aug 25, 2012
author    : bulent.kopuklu
email     : bulent.kopuklu@gmx.com.tr

Copyright (C) 2010 by Nekton
Please read the license agreement in license.txt
'''
import os
import deflib
import subprocess
from defext import gnucc
import sys

__configure__   = ["configure", "build", "install", "make"]
__build__       = ["build", "install", "make"]
__package__     = ["install", "make"]
__all__         = ["make"]

def get_prefix(cntx):
    return os.path.join(cntx.output, "__install__")

def initialize(cntx):
    if not hasattr(cntx.env, "gnucc_compiler"):
        gnucc.init_toolchains(cntx)
        gnucc.init_dependencies(cntx)

    try:
        cmds = ["automake", "--version"]
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.wait():
            raise Exception(stderr)
        v = stdout.split("\n")[0]
        deflib.logtool("automake", v) 
    except:
        raise Exception("automake not found in your system !!!")


def configure(cntx, *k, **kw):
    config_path = os.path.abspath(kw.get("script", ""))
    if os.path.isdir(config_path):
        config_path += os.path.sep + "configure"
    if not os.path.exists(config_path):
        raise Exception("configure script not found !!! <" + config_path + ">")

    build_dir = kw.get("cwd", cntx.output)
    host = kw.get("host", cntx.env.host)
    build = kw.get("build", cntx.env.build)
    prefix = kw.get("prefix", None)
    eprefix = kw.get("eprefix", None)
    
    args = kw.get("args", None)
    if args:
        if not isinstance(args, (list, tuple)):
            args = [args]

    if not os.path.exists(build_dir):
        try:
            os.makedirs(build_dir)
        except:
            pass
    
    env = dict(os.environ)
    _env = kw.get("env", None)
    if _env:
        if not isinstance(_env, dict):
            raise
        env.update(_env)
        if not _env.has_key("PATH"):
            if cntx.env.gnucc_compiler.has_key("path"):
                env["PATH"] = os.path.join(cntx.env.gnucc_compiler["path"], "bin") + ":" + env.get("PATH")
    else:
        if cntx.env.gnucc_compiler.has_key("path"):
            env["PATH"] = os.path.join(cntx.env.gnucc_compiler["path"], "bin") + ":" + env.get("PATH")
            
    task =  Configure(config_path, build_dir, env, host, build, prefix, eprefix, args)
    cntx.env.automake = {"prefix": prefix, "eprefix": eprefix, "cwd": build_dir }
    return (task, None)

def build(cntx, *k, **kw):
    return make(cntx, *k, **kw)

def install(cntx, *k, **kw):
    prefix = kw.get("prefix", cntx.env.automake["prefix"])
    eprefix = kw.get("eprefix", cntx.env.automake["eprefix"])
    
    opts = kw.get("args", None)
    if opts:
        if not isinstance(opts, (list, tuple)):
            opts = [opts]
        for o in opts:
            if isinstance(o, str):
                if o.find("prefix=") == 0:
                    if prefix == None:
                        prefix = o[len("prefix="):]
                    opts.remove(o)
                elif o.find("eprefix=") == 0:
                    if eprefix == None:
                        eprefix = o[len("eprefix="):]
                    opts.remove(o)

    else:
        opts = [] 

    if not prefix:
        prefix = get_prefix(cntx)
    opts.insert(0, "prefix="+prefix)

    if not eprefix:
        eprefix = prefix
    opts.insert(1, "exec_prefix="+eprefix)
        
    t, f = make(cntx, target=kw.get("target", "install"), args=opts)

    return t, (prefix, eprefix) 

def make(cntx, *k, **kw):
    cwd = cntx.env.automake["cwd"]
    if not os.path.exists(os.path.join(cwd, "Makefile")):
        raise Exception("Makefile does not exists. Please run configure")
    
    rules = kw.get("target", None)
    if rules:
        if not isinstance(rules, (list, tuple)):
            rules = [rules]

    args = kw.get("args", None)
    if args:
        if not isinstance(args, (list, tuple)):
            args = [args]
    
    env = dict(os.environ)
    _env = kw.get("env", None)
    if _env:
        if not isinstance(_env, dict):
            raise
        env.update(_env)
        if not _env.has_key("PATH"):
            if cntx.env.gnucc_compiler.has_key("path"):
                env["PATH"] = os.path.join(cntx.env.gnucc_compiler["path"], "bin") + ":" + env.get("PATH")
    else:
        if cntx.env.gnucc_compiler.has_key("path"):
            env["PATH"] = os.path.join(cntx.env.gnucc_compiler["path"], "bin") + ":" + env.get("PATH")
    
    return Make(cwd, rules, args, env), None

class Configure(deflib.task.Object):
    def __init__(self, config_path, build_dir, env, host, build, prefix, eprefix, args):
        super(Configure, self).__init__()
        self.config_path = config_path
        self.build_dir = build_dir
        self.host = host
        self.build = build
        self.env = env
        self.prefix = prefix
        self.eprefix = eprefix
        self.args = args

    def run(self):
        cmds = [self.config_path]
        if self.host and len(self.host) > 0:
            cmds.append("--host=" + self.host)
        if self.host and len(self.host) > 0:
            cmds.append("--build=" + self.build)
        if self.prefix:
            cmds.append("--prefix=" + self.prefix)
        if self.eprefix:
            cmds.append("--exec-prefix=" + self.eprefix)
        if self.args:
            cmds.extend(self.args)
        deflib.logtool("automake", "configure " + self.build_dir)

        sys.stdout.write("\x1b[2m")
        sys.stdout.flush()
        
        try:
            p = subprocess.Popen(cmds, 
                                 stdout=sys.stdout, 
                                 stderr=subprocess.PIPE, 
                                 cwd=self.build_dir, 
                                 env=self.env
                                 )

            stderr = p.communicate()[1]
            if p.wait():
                raise Exception(stderr)
        finally:
            sys.stdout.write("\x1b[0m")            
            sys.stdout.flush()
            
class Make(deflib.task.Object):
    def __init__(self, build_dir, rules, args, env):
        super(Make, self).__init__()
        self.build_dir = build_dir
        self.rules = rules
        self.args = args
        self.env = env
        
    def run(self):
        cmds = ["make"]
        strlog = "make "
                    
        if self.rules:
            cmds.extend(self.rules)
            strlog += str(self.rules)

        if self.args:
            cmds.extend(self.args)
            strlog += str(self.args)
            
        deflib.logtool("automake", strlog) 

        sys.stdout.write("\x1b[2m")
        sys.stdout.flush()

        try:
            p = subprocess.Popen(cmds,  
                                 stdout=sys.stdout, 
                                 stderr=subprocess.PIPE, 
                                 cwd=self.build_dir,
                                 env=self.env
                                 )

            stderr = p.communicate()[1]
            if p.wait():
                raise Exception(stderr)
        finally:
            sys.stdout.write("\x1b[0m")            
            sys.stdout.flush()
