'''
Created on Sep 12, 2012

@author: bulentk
'''
import os
import prj
import deflib
import imp

SCRIPT_FILE_NAME = "defne"

class Script(object):
    def __init__(self, mdl, path, root):
        self.mdl = mdl
        self.path = path
        self.root = root

    def __getattr__(self, name):
        return getattr(self.mdl, name)


def load(path, parent = None):
    filepath = os.path.join(path, SCRIPT_FILE_NAME)
    mdl = imp.new_module(filepath)
    if not parent:
        setattr(mdl, "Project", prj.create)
    setattr(mdl, "deflib", deflib)
    return Script(deflib.load_module(filepath, mdl), path, parent)

def __default__(cntx):
    pass

def execute(s, cntx, **kw):
    cws = None
    if cntx.cws:
        cws = cntx.cws
    cntx.cws = s

    ctor = kw.get("ctor", None)
    dtor = kw.get("dtor", None)
    target = getattr(s, cntx.cmd_name, None)
    if target:
        setattr(s.mdl, "default", kw.get("default", __default__))
    else:
        target = kw.get("default", None)
        
    if not target:
        raise Exception("defne has no target " + cntx.cmd_name)
    
    if ctor:
        ctor(cntx)
    target(cntx)
    if dtor:
        dtor(cntx)

    if cws:
        cntx.cws = cws
    
    
#def execute(s, cntx):
#
#    try:
#        getattr(s, cntx.cmd_name)(cntx)
#    except AttributeError:
#        pass
#    cws = None
#    if cntx.cws:
#        cws = cntx.cws
#    cntx.cws = s
#        
#    if cws:
#        cntx.cws = cws
#
#def options(s):
#    fromctor = None
#    fromscript = None
#    try:
#        ctor = __import__("constructer") #
#        try: 
#            fromctor = getattr(ctor, SCRIPT_OPT_KEYWORD)
#        except AttributeError:
#            pass
#    except ImportError:
#        pass
#        
#    try:
#        fromscript = getattr(s.mdl, SCRIPT_OPT_KEYWORD)
#    except AttributeError:
#        pass
#
#    fromdef = getattr(defcmd.options, "default")
#    
#    def __options(cntx):
#        fromdef(cntx)
#        if fromctor:
#            fromctor(cntx)
#        if fromscript:
#            fromscript(cntx)
#             
#    setattr(s, SCRIPT_OPT_KEYWORD, __options)
#    
#def configure(s):
#    fromscript = None
#    try:
#        fromscript = getattr(s.mdl, SCRIPT_CFG_KEYWORD)
#    except AttributeError:
#        pass
#    fromdef = getattr(defcmd.configure, "default")
#    
#    def __configure(cntx):
#        fromdef(cntx)
#        if fromscript:
#            fromscript(cntx)
#
#    setattr(s, SCRIPT_CFG_KEYWORD, __configure)
#    
#def build(s):
#    fromscript = None
#    try:
#        fromscript = getattr(s.mdl, SCRIPT_BUILD_KEYWORD)
#    except AttributeError:
#        pass
#    fromdef = getattr(defcmd.build, "default")
#    
#    def __build(cntx):
#        if fromscript:
#            setattr(s.mdl, "default", fromdef)
#            fromscript(cntx)
#        else:
#            fromdef(cntx) 
#
#    setattr(s, SCRIPT_BUILD_KEYWORD, __build)
#
#def clean(s):
#    fromscript = None
#    try:
#        fromscript = getattr(s.mdl, SCRIPT_CLEAN_KEYWORD)
#    except AttributeError:
#        pass
#    fromdef = getattr(defcmd.clean, "default")
#    
#    def __clean(cntx):
#        if fromscript:
#            setattr(s.mdl, "default", fromdef)
#            fromscript(cntx)
#        else:
#            fromdef(cntx) 
#
#    setattr(s, SCRIPT_CLEAN_KEYWORD, __clean)
#
#def package(s):
#    fromscript = None
#    try:
#        fromscript = getattr(s.mdl, SCRIPT_PACKAGE_KEYWORD)
#    except AttributeError:
#        pass
#    fromdef = getattr(defcmd.package, "default")
#    
#    def  __package(cntx):
#        if fromscript:
#            setattr(s.mdl, "default", fromdef)
#            fromscript(cntx)
#        else:
#            fromdef(cntx) 
#    
#    setattr(s, SCRIPT_PACKAGE_KEYWORD, __package)
#    
#def publish(s):
#    fromscript = None
#    try:
#        fromscript = getattr(s.mdl, SCRIPT_PUBLISH_KEYWORD)
#    except AttributeError:
#        pass
#    fromdef = getattr(defcmd.publish, "default")
#    
#    def __publish(cntx):
#        if fromscript:
#            setattr(s.mdl, "default", fromdef)
#            fromscript(cntx)
#        else:
#            fromdef(cntx) 
#
#    setattr(s, SCRIPT_PUBLISH_KEYWORD, __publish)
