#! /usr/bin/env python


'''
Created on Sep 12, 2012

@author: bulentk
'''


import sys
import engine
import os
import traceback
import deflib
import defext
import defrepo

def main():
    iRslt = int(0)
    tick = deflib.tick_count()  
    try:
        for p in defext.__path__:
            sys.path.append(p)
        for p in defrepo.__path__:
            sys.path.append(p)

        sys.path.append("/etc/def")
        engine.launch()
    except Exception, e:
        deflib.logger.err("<%s@%s> %s" % (engine.cwc, os.getcwd(), e))
        iRslt = -1
##        traceback.print_exc(file=sys.stdout)
    deflib.logger.log(deflib.logger.bold(deflib.logger.green("\ndone ... %s" % (deflib.tick_count(tick)))))
    sys.exit(iRslt)


if __name__ == '__main__':
    main()