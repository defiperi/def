'''
Created on Jan 23, 2013

@author: bulentk
'''

import engine
import deflib
import os
import shutil

def run_cleanall(s):
    engine.heading("cleanall")
    
    engine.scripting(s, "clean")
    
    deflib.logtool("rm", "outputs", False)
    if os.path.exists(engine.settings.output):
        shutil.rmtree(engine.settings.output)
    deflib.logtool("rm", "filesys", False)
    if os.path.exists(engine.pkg_filesystem()):
        shutil.rmtree(engine.pkg_filesystem())

def run_all(s):
    cpus = engine.settings.machine.toolchains.keys()
    
    for cpu in cpus:
        engine.opt.target_cpu = cpu
        for cfg in [ "release", "debug" ]:
            engine.opt.build_config = cfg
            for c in [ "configure", "build", "package", "publish" ]:
                engine.cwc = c
                engine.scripting(s, c)


def run_redownload(s):
    pass

def run_reinstall(s):
    pass

lists = {
            "all":run_all, 
            "cleanall":run_cleanall, 
            "redownload":run_redownload, 
            "reinstall":run_reinstall
        }
