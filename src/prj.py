'''
Created on Oct 2, 2012

@author: bulentk
'''
import engine
import deflib
from operator import contains

def clear_duplicate(lst):
    group = { }    
    for p in lst:
        k = p.prj + p.name
        if group.has_key(k):
            l = group[k]
            if not contains(l, p):
                group[k].append(p)
        else:
            group[k] = [ p ]

    rslt = []
    for n in group.keys():
        rslt.extend(group.get(n))
    return rslt

class Dependency(object):
    def __init__(self, prj, pkg, version, build_config, scope, url):
        self.prj = prj
        self.name = pkg
        self.build_config = build_config 
        self.version = deflib.pkg.Version(version)
        self.scope = scope

    def __str__(self):
        return "%s(%s-%s) %s" % (self.prj, self.name, self.build_config if self.build_config else engine.opt.build_config, self.version)
    
    def __cmp__(self, other):
        if self.prj != self.other.prj:
            return -1
        if self.name != self.other.name:
            return -1
        if self.build != self.other.build:
            return -1
        return cmp(self.version, other.version)

class Project(object):
    def __init__(self, name):
        self.name = name
        self.version = "1.0.1"
        self.requires = []
        self.alldeppkgs = []
    
    def add_dep(self, prj, pkg=None, version = "", build_config="", scope="product", url=None):
        name = pkg if pkg else prj;     
        d = Dependency(prj, name, version, build_config, scope, url)
        self.requires.append(d)
        
        return d
    
    def get_dep(self, prj, pkg=None):
        name = pkg if pkg else prj;
        for d in self.requires:
            if d.scope == "product" and d.prj == prj and d.name == name:
                return d;
        return None
            
    def __add_deppkgs__(self, pkgs):
        self.alldeppkgs.extend(pkgs)
        self.alldeppkgs = clear_duplicate(self.alldeppkgs)

    def __str__(self):
        s = "%s(%s)" % (self.name, self.version)
        if self.requires:
            s += "[ "
            for d in self.requires:
                s += str(d) + " "
            s += "]"
        return s    
            

def create(name = ""):
    if engine.project:
        del engine.project
    engine.project = Project(name)
    return engine.project
